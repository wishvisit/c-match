<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'matching' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '/xS: k1}#85_l<p|gF?[R$99<`am)P#//hKU=ZcI8g|-MtQ(H0Jq{(?US6z6kj2;' );
define( 'SECURE_AUTH_KEY',  ' bDA3+f#`j1c@jd%BX1U(D|LrE@Hn.<B4$Y]A=7ctW:,[`0Oun$!OO1t>LWi|$ H' );
define( 'LOGGED_IN_KEY',    '?d%d@^wWDxkej@.ve0+?Kei*ayJ?((m1CEKGc+*Lb9RH<8Lr,]g*E4FhixQi3`dE' );
define( 'NONCE_KEY',        'M,e&{6VXBI|[TH19m/Y{ZhuwH@;B/0S51;oU[v{_We CHFog#hQy* ]hD|q-X/?v' );
define( 'AUTH_SALT',        'BIQa>~[t+]k5Tr$9IZqdlg7VL*ef_`q)#???%X-DHIn,OZ/J_iC>L~ R<IlD7]ws' );
define( 'SECURE_AUTH_SALT', '1#tOR062yz{I{X/Q(gBFFTZnBMO,:0w6y{-a-*tiQ}~Cw2- XdfotQbR$FhKyw/L' );
define( 'LOGGED_IN_SALT',   'pZ!U&Kf]gzex@jdJ&dsp6ZpvY3vDqRe@9+*I>SUq0+152`J yY2oZYeME1e0)RpB' );
define( 'NONCE_SALT',       'KNt~j~mpqZ:ww2%6ZrPVSyq^]PPv:|hKg(vWlE$fBI]_| s5#5Uii;!mDy5;7A}Q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
