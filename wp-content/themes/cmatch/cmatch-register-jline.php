<?php
/**
* Template Name: J Line
 *
 */
get_template_part( 'template-parts/cmatch-header' );?>
<main id="main-container" class="home-page-container form-thai">
	<?php get_template_part( 'template-parts/cmatch-menu' );?>
	<!--<section id="register-page-title" class="full-page-background">
         <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="event-title">
                      
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/dummy-thankyou.png" alt="dummy bg" />
                        <?php /* change by script
                        <p class="font-prompt">Welcome to</p>
                        <h2 class="font-prompt title">SAKURA NO SATO</p>
                        <p class="font-prompt sub-title">Event</p> */?>
                    </div>
                </div>
            </div>
        </div>
	</section>-->
	<style>
		.tags-look .tagify__dropdown__item{
			display: inline-block;
			border-radius: 3px;
			padding: .3em .5em;
			border: 1px solid #CCC;
			background: #F3F3F3;
			margin: .2em;
			font-size: .85em;
			color: black;
			transition: 0s;
		}

		.tags-look .tagify__dropdown__item--active{
			color: black;
		}

		.tags-look .tagify__dropdown__item:hover{
			background: lightyellow;
			border-color: gold;
		}
	</style>
	<?php if(!is_user_logged_in()):?>
	<div class="container p-0" style=" -moz-box-shadow: 0px 5px 200px #c8f0ff;
  -webkit-box-shadow: 0px 5px 200px #c8f0ff;
  box-shadow: 0px 5px 200px #c8f0ff;">
		<div class="">
			<img src="<?php echo get_template_directory_uri();?>/assets/images/jlinebanner.png">
			<div class="row p-5">
				<div class="col-12
				
				]">
					<section id="register-login">
						<h2 class="login-notice text-center lang-tag" data-lang="before_login">ลงทะเบียนที่นี่</h2>
						<link type="text/css" rel="stylesheet"
							href="https://cdn.firebase.com/libs/firebaseui/3.5.2/firebaseui.css" />
						<div id="firebaseui-auth-container"></div>
						<input type="hidden" id="event-id" value="<?php echo get_the_ID();?>">
					</section>
				</div>
			</div>

		</div>


	</div>
	<?php else:?>
	<?php if($current_user){
            $company_id = $current_user->ID;
            $company_data = is_company_registered($current_user->ID);
            $company_name = $company_type = $company_email = $company_description = $company_website = $contact_address = $contact_phone = $interest_description= $contact_name = $product_target = $product_description = $product_category = $product = $product_quantity  = $product_image = "";
            if($company_data){
				$company_name 			= $company_data->company_name;
				$company_type			= $company_data->company_type;
				$company_description	= $company_data->company_description;
				$company_email 			= $company_data->company_email;

				//product
				$product	            = $company_data->product;
				$product_category		= $company_data->product_category;
				$product_description	= $company_data->product_description;
				$product_quantity       = $company_data->product_quantity;
				$product_image          = $company_data->product_image;
				$product_target         = $company_data->product_target;

				//contact details
				$contact_name			= $company_data->contact_name;
				$contact_address		= $company_data->contact_address;
				$contact_phone			= $company_data->contact_phone;
				$company_website        = $company_data->company_website;
				$interest_description	= $company_data->interest_description;
			}			
		
        }?>
	<section id="home-title" class="full-page-background register-detail container">
		<form class="center" id="register-form">
			<div class="prompt-regular gray1">
				<!-- bg 1 -->
				<div class="center bg1">
					<section class="row m-5">
						<div class="col-12 text-center">
							<h2>เปิดโอกาสในการทำธุรกิจระหว่างประเทศไทย-ญี่ปุ่น <br>ลงทะเบียนเพื่อหาคู่ค้าที่สนใจธุรกิจของคุณกับ CMATCH ได้ที่นี่!</h2>
						</div>
					</section>
					<section class="section-1">
						<div class="q1 p-0 row">
							<div class="col-12 form-group ">
								<h2 class="form-section-title prompt-semi lang-tag" data-lang="form_company_data_title">
									ข้อมูลบริษัทของท่าน</h2>
								<input type="hidden" id="companyid" value="<?php echo $company_id;?>">
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="companynames" data-lang="company_name">ชื่อบริษัท</label>
								<input type="text" class="form-control lang-tag validate-input" data-lang="company_name"
									name="companyname" id="companyname" value="<?php echo $company_name;?>">
								<small class="text-danger hidden error-flag companyname">กรุณาระบุชื่อบริษัท</small>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="company_type" data-lang="company_type">ประเภทบริษัท</label>
								<select id="company_type" class="form-control">
									<option value="" disabled selected></option>
									<option value="Manufacturer">Manufacturer</option>
									<option value="Wholesaler">Wholesaler</option>
									<option value="Reseller">Reseller</option>
									<option value="Retailer">Retailer</option>
									<option value="Distributor">Distributor</option>
									<option value="Trader">Trader</option>
								</select>
								<small class="text-danger hidden error-flag companytype">กรุณาเลือกประเภทบริษัท</small>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="companydescription"
									data-lang="company_description">อธิบายเกี่ยวกับบริษัทสั้นๆ</label>
								<textarea class="form-control" id="company-description"
									rows="3"><?php echo $company_description;?></textarea>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="contactwebsite"
									data-lang="contactwebsite">เว็บไซต์ของบริษัท</label>
								<input type="text" class="form-control validate-input" name="contactwebsite"
									id="contactwebsite" value="<?php echo $company_website;?>">
								<small class="text-danger hidden error-flag contactwebsite">กรุณาระบุlink เว็บไซต์บริษัท
									หรือ link page social media</small>
							</div>
							

						</div>
					</section>
					<!-- Section-01 -->
					<hr>
					<section class="section-2">
						<div class="p-0 row">
							<div class="col-12 form-group ">
								<h2 class="form-section-title prompt-semi lang-tag" data-lang="form_product_details">
									ข้อมูลผลิตภัณฑ์</h2>
							</div>
								<style>
								.tagify__input {min-height: 28px;}
								</style>
							<div class="col-12 form-group">
								<label class="lang-tag" for="product" data-lang="product">ผลิตภัณฑ์ (ภาษาอังกฤษ)</label>
								<input  type="text" id="product" class="form-control validate-input" data-lang="product"
									name="product" placeholder='ผลิตภัณฑ์' value="<?php echo $product;?>">
								<small class="">กรุณาเลือกชื่อผลิตภัณฑ์จากรายการ หรือเพ่ิมใหม่เป็นภาษาอังกฤษเพื่อสะดวกต่อการประสานงาน</small>
								<small class="text-danger hidden error-flag product">กรุณาระบุ ผลิตภัณฑ์</small>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="product_category"
									data-lang="product_category">หมวดผลิตภัณฑ์ (Product Category)(ภาษาอังกฤษ)</label>
								<input type="text" class="form-control lang-tag validate-input"
									data-lang="product_category" name="product_category" id="product-category" placeholder="หมวดผลิตภัณฑ์"
									value="<?php echo $product_category;?>">
								<small class="">กรุณาเลือกหมวดผลิตภัณฑ์จากรายการ  หรือเพิ่มใหม่เป็นภาษาอังกฤษเพื่อสะดวกต่อการประสานงาน</small>
								<small class="text-danger hidden error-flag product_category">กรุณาระบุ
									หมวดผลิตภัณฑ์</small>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="product_description"
									data-lang="product_description">คุณประโยชน์สำคัญของผลิตภัณฑ์ (product core
									benefit)</label>
								<textarea class="form-control" id="product-description"
									rows="3"><?php echo $product_description;?></textarea>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="product_quantity"
									data-lang="product_quantity">ค้าส่ง/ปลีก</label>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="product_quantity"
										id="product-quantity1" value="retail">
									<label class="form-check-label" for="product_quantity1">
										Retail / ค้าปลีก
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="product_quantity"
										id="product_quantity2" value="wholesale">
									<label class="form-check-label" for="product_quantity2">
										Wholesale / ค้าส่ง
									</label>
								</div>
								<small class="text-danger hidden error-flag companytype">กรุณาเลือกประเภทบริษัท</small>
							</div>
							<div class="col-12 form-group">
								<label for="product_image">ภาพตัวอย่างผลิตภัณฑ์ (1 รูป)</label>
								<input type="file" class="form-control-file" id="product_image"
									accept=".jpg,.jpeg,.png,.pdf">
								<small class="text-muted">ไฟล์นามสกุล .jpg .jpeg .png และ .pdf</small>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="product_target" data-lang="product_target">กลุ่มเป้าหมาย
									(Target Market)</label>
								<input type="text" class="form-control lang-tag validate-input"
									data-lang="product_target" name="product_target" id="product-target"
									value="<?php echo $product_target;?>">
							</div>
						</div>

					</section>
					<hr>
					<!-- Section-02 -->
					<section class="section-2">
						<div class="row">
							<div class="col-12 form-group q6 p-0">
								<h2 class="form-section-title lang-tag" data-lang="contact_section">ข้อมูลผู้ติดต่อ</h2>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="contactname" data-lang="contact_name_label">ชื่อ</label>
								<input type="text" class="form-control lang-tag validate-input" data-lang="contact_name"
									placeholder="" name="contact_name" id="contactname" value="<?php echo $contact_name;?>">
								<small class="text-danger hidden error-flag contactname">กรุณาระบุขื่อผู้ติดต่อ</small>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="contact_address"
									data-lang="contact_address">ที่อยู่บริษัท</label>
								<input type="text" class="form-control lang-tag validate-input" data-lang="contact_address"
									placeholder="" name="contact_address" id="contactaddress"
									value="<?php echo $contact_address;?>">
								<small
									class="text-danger hidden error-flag contactaddress">กรุณาระบุที่อยู่บริษัท</small>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="contactphone" data-lang="contact_phone">เบอร์โทรศัพท์</label>
								<input type="text" class="form-control validate-input" name="contact_phone"
									id="contactphone" value="<?php echo $contact_phone;?>">
								<small
									class="text-danger hidden error-flag contactphone">กรุณาระบุเบอร์โทรศัพท์ของผู้ติดต่อ</small>
							</div>
							<div class="col-12 form-group">
								<label for="companyemail" data-lang="companyemail"
									class="lang-tag">อีเมลที่สามารถติดต่อได้</label>
								<input type="email" class="form-control validate-input" name="companyemail"
									id="companyemail" value="<?php echo $company_email;?>">
								<small class="text-danger hidden error-flag companyemail">กรุณาระบุอีเมลบริษัท</small>
							</div>
						</div>
					</section>
					<!-- Section 2 -->
				</div>
			</div>
			<!-- bg 1 -->
			<div class="bg2">
				<div class="center prompt-regular gray1">
					<hr>
					<!-- Section 4 -->
					<section class="row section-4">
						<div class="col-12 form-group q10">
							<h2 class="form-section-title lang-tag" data-lang="other_request_title">ความช่วยเหลือและการสนับสนุนที่ต้องการเพิ่มเติม
								(special request)</h2>
						</div>
						<div class="col-12 form-group">
							<div id="interest-group">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="strategy_market" id="defaultCheck1">
									<label class="form-check-label" for="defaultCheck1">
										วางแผนกลยุทธ์ในการเข้าสู่ตลาด
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="business_consult" id="defaultCheck2">
									<label class="form-check-label" for="defaultCheck2">
										รับคำปรึกษาด้านธุรกิจ
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="research_target" id="defaultCheck3">
									<label class="form-check-label" for="defaultCheck3">
										ทำวิจัยและเก็บข้อมูลตลาดกลุ่มเป้าหมาย
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="data_analyze" id="defaultCheck4">
									<label class="form-check-label" for="defaultCheck4">
										วิเคราะห์ข้อมูลด้านธุรกิจ
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="develop_product" id="defaultCheck5">
									<label class="form-check-label" for="defaultCheck5">
									การพัฒนาสินค้าและการตลาด
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="media_management" id="defaultCheck6">
									<label class="form-check-label" for="defaultCheck6">
									การบริหารจัดการด้านสื่อ
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="marketing_event" id="defaultCheck7">
									<label class="form-check-label" for="defaultCheck7">
									การจัดการอีเวนท์และกิจกรรมการตลาด
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="package_design" id="defaultCheck8">
									<label class="form-check-label" for="defaultCheck8">
									การออกแบบกราฟฟิคให้กับสินค้าและบรรจุภัณฑ์
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="website_management" id="defaultCheck9">
									<label class="form-check-label" for="defaultCheck9">
									การจัดการช่องทางออนไลน์และเว็บไซต์
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="socialmedia_management" id="defaultCheck10">
									<label class="form-check-label" for="defaultCheck10">
									การบริหารจัดการสื่อและเนื้อหาบนโลกโซเชียล
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="translate_service" id="defaultCheck11">
									<label class="form-check-label" for="defaultCheck11">
									การประสานงานด้านภาษาและการแปล
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="accountant_service" id="defaultCheck12">
									<label class="form-check-label" for="defaultCheck12">
									รับคำปรึกษาในการทำบัญชีและการจัดเตรียมเอกสาร
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="law_service" id="defaultCheck13">
									<label class="form-check-label" for="defaultCheck13">
									รับคำปรึกษาเกี่ยวกับกฎหมายและกฎระเบียบ
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" data-check="location_service" id="defaultCheck14">
									<label class="form-check-label" for="defaultCheck14">
									ค้นหาทำเลที่ตั้งในการเปิดกิจการในประเทศญี่ปุ่น
									</label>
								</div>
							</div>
						</div>
						<?php if($company_data):?>
						<div id="form-validates" class="btn btn-primary mt-5 txt-20 center mb-4 lang-tag"
							data-lang="edit">แก้ไข</div>
						<?php else:?>
						<div id="form-validates" class="btn btn-primary mt-5 txt-20 center mb-4 lang-tag"
							data-lang="submit">ยืนยัน</div>
						<?php endif;?>
					</section>
					<!-- Section 4 -->
				</div>
			</div>
		</form>
	</section>
	<?php endif;?>
</main>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="col-12 modal-header">
				<button type="button" class="close right mr-3" data-dismiss="modal">&times;</button>
				<h4 class="modal-title center">ตรวจสอบข้อมูล</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-4 lang-tag" data-lang="company_name">ชื่อบริษัท</div>
						<div id="" class="col-8 summary-answer" data-key="company_name"></div>
						<div class="col-4 lang-tag" data-lang="company_type">ประเภทบริษัท</div>
						<div id="" class="col-8 summary-answer" data-key="company_type"></div>
						<div class="col-4 lang-tag" data-lang="company_description">อธิบายเกี่ยวกับบริษัทสั้นๆ</div>
						<div id="" class="col-8 summary-answer" data-key="company_description"></div>
						<div class="col-4 lang-tag" data-lang="companyemail">อีเมลกลางของบริษัท</div>
						<div id="" class="col-8 summary-answer" data-key="company_email"></div>

						<div class="col-4 lang-tag" data-lang="product">สินค้่า</div>
						<div id="" class="col-8 summary-answer" data-key="product"></div>
						<div class="col-4 lang-tag" data-lang="product">รูปสินค้่า</div>
						<div id="" class="col-8 " data-key=""><img scr="" id="productimgoutput" alt="product output"/></div>
						<div class="col-4 lang-tag" data-lang="product_category">ประเภทสินค้่า</div>
						<div id="" class="col-8 summary-answer" data-key="product_category"></div>
						<div class="col-4 lang-tag" data-lang="product_description">คุณประโยชน์สำคัญของผลิตภัณฑ์</div>
						<div id="" class="col-8 summary-answer" data-key="product_description"></div>
						<div class="col-4 lang-tag" data-lang="product_quantity">ค้าส่ง/ปลีก</div>
						<div id="" class="col-8 summary-answer" data-key="product_quantity"></div>
						<div class="col-4 lang-tag" data-lang="product_target">กลุ่มเป้าหมาย</div>
						<div id="" class="col-8 summary-answer" data-key="product_target"></div>

						<div class="col-4 lang-tag" data-lang="contact_name">ข้อมูลผู้ติดต่อ</div>
						<div id="" class="col-8 summary-answer" data-key="contact_name"></div>
						<div class="col-4 lang-tag" data-lang="contact_address">ที่อยู่บริษัท</div>
						<div id="" class="col-8 summary-answer" data-key="contact_address"></div>
						<div class="col-4 lang-tag" data-lang="contact_phone">เบอร์โทรศัพท์</div>
						<div id="" class="col-8 summary-answer" data-key="contact_phone"></div>
						<div class="col-4 lang-tag" data-lang="companywebsite">เว็บไซต์บริษัท</div>
						<div id="" class="col-8 summary-answer" data-key="company_website"></div>
						<div class="col-4 lang-tag" data-lang="other_request_title">ความต้องการอื่นๆ</div>
						<div id="" class="col-8 summary-answer" data-key="interest_description"></div>
					</div>
				</div>
			</div>
			<div class="row modal-footer m-3">
				<button type="button" class="btn btn-default mt-5 center mb-4 txt-20"
					data-dismiss="modal">ยกเลิก</button>
				<?php if($company_data):?>
				<button type="submit" id="form-edits" class="btn btn-primary mt-5 txt-20 center mb-4 lang-tag"
					data-lang="edit">แก้ไข</button>
				<?php else:?>
				<button type="submit" id="form-submits" class="btn btn-primary mt-5 txt-20 center mb-4 lang-tag"
					data-lang="submit">ยืนยัน</button>
				<?php endif;?>
			</div>
		</div>

	</div>
</div>
<?php get_template_part( 'template-parts/cmatch-footer' );?>