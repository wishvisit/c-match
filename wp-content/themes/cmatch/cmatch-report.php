<?php
/**
* Template Name: Report list
 *
 */
get_template_part( 'template-parts/cmatch-header' );?>
<main id="main-container" class="home-page-container prompt-regular">
	<?php get_template_part( 'template-parts/cmatch-menu' );?>

	<?php  if(is_user_logged_in() && current_user_can('administrator'))://check login and admin
			$all_item = get_report();
			//print_r($all_item);
		?>

	<section class="pt-5">
		<div class="container report">
			<div class="col-12">
				<table class="table" id="the-table">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">ประเทศ</th>
							<th scope="col">ขื่อบริษัท</th>
							<th scope="col">เบอร์โทรศัพท์บริษัท</th>
							<th scope="col">emailบริษัท</th>
							<th scope="col">webบริษัท</th>
							<th scope="col">ประเภทบริษัท</th>
							<th scope="col">ประเภทบริษัท อื่นๆ</th>
							<th scope="col">ประเภทลูกค้า</th>
							<th scope="col">ประเภทลูกค้า อื่นๆ</th>
							<th scope="col">ระดับงบประมาณของลูกค้า</th>
							<th scope="col">อธิบายเกี่ยวกับบริษัทสั้นๆ</th>
							<th scope="col">ขื่อผู้ติดต่อ</th>
							<th scope="col">ตำแหน่งติดต่อ</th>
							<th scope="col">เบอร์โทรศัพท์ผู้ติดต่อ</th>
							<th scope="col">emailผู้ติดต่อ</th>			
							<th scope="col">ประเภทบริษัทฝั่งญี่ปุ่น</th>
							<th scope="col">ประเภทบริษัทฝั่งญี่ปุ่น อื่นๆ</th>
							<th scope="col">สนใจข้อมูลเกี่ยวกับอะไร</th>
							<th scope="col">สนใจข้อมูลเกี่ยวกับอะไร อื่นๆ</th>
							<th scope="col">ระดับราคา</th>
							<th scope="col">special request</th>
							
						</tr>
					</thead>
					<tbody>
						<?php foreach($all_item as $item):?>
						<tr>
							<td><?php echo $item['company_id'];?></td>
							<td><?php echo $item['company_country'];?></td>
							<td><?php echo $item['company_name'];?></td>
							<td><?php echo $item['company_phone'];?></td>
							<td><?php echo $item['company_email'];?></td>
							<td><?php echo $item['company_website'];?></td>
							<td>
								<?php if ($item['company_type']):?>
								<?php $company_type = json_decode($item['company_type'],true);?>
								<?php foreach($company_type as $key => $value){
										echo $key."\n";
									}?>
								<?php endif;?>
							</td>
							<td><?php echo $item['company_type_other'];?></td>
							<td><?php if ($item['company_customer_type']):?><?php $company_customer_type = json_decode($item['company_customer_type'],true);?><?php foreach($company_customer_type as $key => $value){
										echo $key."\n";
									}?><?php endif;?></td>
							<td><?php echo $item['company_customer_other'];?></td>
							<td><p><?php if ($item['your_budget']):?><?php $your_budget = json_decode($item['your_budget'],true);?><?php foreach($your_budget as $key => $value){
										echo $key."\n";
									}?><?php endif;?></p></td>
							<td><?php echo $item['company_description'];?></td>
							<?php /* contact section */;?>
							<td><?php echo $item['contact_name'];?></td>
							<td><?php echo $item['contact_position'];?></td>
							<td><?php echo $item['contact_phone'];?></td>
							<td><?php echo $item['contact_email'];?></td>
							<?php /* interest section */;?>
							<td>
								<p>
									<?php if ($item['interest_company']):?>
									<?php $interest_company = json_decode($item['interest_company'],true);?>
									<?php foreach($interest_company as $key => $value){
										echo $key."\n";
									}?>
									<?php endif;?><p>
							</td>
							<td><?php echo $item['interest_company_other'];?></td>
							<td>
								<p>
									<?php if ($item['interest_activity']):?>
									<?php $interest_activity = json_decode($item['interest_activity'],true);?>
									<?php foreach($interest_activity as $key => $value){
										echo $key."\n";
									}?>
									<?php endif;?><p>
							</td>
							<td><?php echo $item["interest_activity_other"];?></td>
							<td>
								<p>
									<?php if ($item['expect_budget']):?>
									<?php $expect_budget = json_decode($item['expect_budget'],true);?>
									<?php foreach($expect_budget as $key => $value){
										echo $key."\n";
									}?>
									<?php endif;?><p>
							</td>
							<td><?php echo $item["interest_description"];?></td>
						</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
	<?php else: //no admin && no login ?>
	<section class="pt-5">
		<div class="container thankyou">
			<div class="row">
				<div class="col-12">
					<?php echo is_admin();?>
					<a href="<?php echo home_url();?>">Back to Home</a>
				</div>
			</div>
		</div>
	</section>
	<?php endif;?>
</main>
<?php get_template_part( 'template-parts/cmatch-footer' );?>