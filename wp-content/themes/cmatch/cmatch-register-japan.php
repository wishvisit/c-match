<?php
/**
* Template Name: Register Japan
 *
 */
get_template_part( 'template-parts/cmatch-header' );?>
<main id="main-container" class="home-page-container form-jp">
    <?php get_template_part( 'template-parts/cmatch-menu' );?>
    <!--<section id="register-page-title" class="full-page-background">
         <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="event-title">
                      
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/dummy-thankyou.png" alt="dummy bg" />
                        <?php /* change by script
                        <p class="font-prompt">Welcome to</p>
                        <h2 class="font-prompt title">SAKURA NO SATO</p>
                        <p class="font-prompt sub-title">Event</p> */?>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <section id="register-bar">
        <div class="container">
           <div class="col-12 col-lg-8 center">
				<div class="row justify-content-md-center">
					<div class="col col-lg-3">
						<img class="thaiflag" src="<?php echo get_template_directory_uri();?>/assets/images/C Match-67.png" alt="Thai Flag" />
					</div>
					<div class="col col-lg-3">
						<img id="step-register" class="step-logo active" src="<?php echo get_template_directory_uri();?>/assets/images/register/C Match-41-100white.png" alt="Thai Flag" />
					</div>
					<div class="col col-lg-3">
						<img id="step-select" class="step-logo" src="<?php echo get_template_directory_uri();?>/assets/images/register/C Match-45.png" alt="Thai Flag" />
					</div>
					<div class="col col-lg-3">
						<img id="step-date" class="step-logo" src="<?php echo get_template_directory_uri();?>/assets/images/register/C Match-46.png" alt="Thai Flag" />
					</div>
				</div>
            </div>
        </div>
    </section>
    <?php if(!is_user_logged_in()):?>
    <section id="register-login">
        <h2 class="login-notice text-center lang-tag" data-lang="before_login">登録前にログインをお願いします。</h2>
        <link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/3.5.2/firebaseui.css" />
        <div id="firebaseui-auth-container"></div>
        <input type="hidden" id="event-id" value="<?php echo get_the_ID();?>">
    </section>
    <?php else:?>
        <?php if($current_user){
            $company_id = $current_user->ID;
            $company_country = "JP";
            $company_data = is_company_registered($current_user->ID);
            $company_name = $company_phone = $company_email = $company_description = $company_website = $contact_position = $contact_phone= $contact_email= $interest_description= $contact_name = $remark = $company_type_other = $company_customer_other = $interest_company_other = $interest_activity_other ="";
            if($company_data){
				$company_name           = $company_data->company_name;
                $company_phone          = $company_data->company_phone;
                $company_email          = $company_data->company_email;
				$company_description    = $company_data->company_description;
				$your_budget			= json_decode($company_data->your_budget);
				$company_type			= json_decode($company_data->company_type);
				$company_type_other		= $company_data->company_type_other;
				$company_customer_type	= json_decode($company_data->company_customer_type);
				$company_customer_other = $company_data->company_customer_other;
                $company_website        = $company_data->company_website;
                $contact_name           = $company_data->contact_name;
                $contact_position       = $company_data->contact_position;
                $contact_phone          = $company_data->contact_phone;
				$contact_email          = $company_data->contact_email;
				$interest_company		= json_decode($company_data->interest_company);
				$interest_company_other = $company_data->interest_company_other;
				$interest_activity		= json_decode($company_data->interest_activity);
				$interest_activity_other= $company_data->interest_activity_other;
				$expect_budget			= json_decode($company_data->expect_budget);
                $interest_description   = $company_data->interest_description;
				$remark                 = $company_data->remark;

				if($company_data->company_country !== $company_country && isset($company_data->company_country)){
					wp_redirect("/thai-company");
					exit();
				}else{
					$showalert = "hidden";
				}
			}
        }?>
    <section id="home-title" class="full-page-background register-detail">
            <form class="col-12 center prompt-regular gray1" id="register-form">
             	<div class="col-12 ">
             		<div class="col-12 col-lg-7 center bg1">
						<!-- Section-1 -->
						<section class="col-12 section-1">
							<div class="col-12 form-group">
								<h2 class="form-section-title prompt-semi lang-tag" data-lang="form_company_data_title">会社情報</h2>
								<input type="hidden" id="companyid" value="<?php echo $company_id;?>">
								<input type="hidden" id="companycountry" value="<?php echo $company_country;?>">
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="companynames" data-lang="company_name">会社名</label>
								<input type="text" class="form-control lang-tag validate-input" data-lang="company_name" 
									name="companyname" id="companyname" value="<?php echo $company_name;?>">
								<small class="text-danger hidden error-flag companyname lang-tag" data-lang="alert_company_name">会社名を入力してください。</small>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="companyphone" data-lang="companyphone">電話番号</label>
								<input type="text" class="form-control validate-input"  name="companyphone"
									id="companyphone" value="<?php echo $company_phone;?>">
								<small class="text-danger hidden error-flag companyphone lang-tag" data-lang="alert_company_phone">会社の電話番号を入力してください。</small>
							</div>
							<div class="col-12 form-group">
								<label for="companyemail" data-lang="companyemail" class="lang-tag">メールアドレス</label>
								<input type="email" class="form-control validate-input" name="companyemail"
									id="companyemail" value="<?php echo $company_email;?>">
								<small class="text-danger hidden error-flag companyemail lang-tag" data-lang="alert_company_email">会社のメールアドレスを入力してください。</small>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="companywebsite" data-lang="companywebsite">会社のウェブサイト</label>
								<input type="text" class="form-control validate-input" name="companywebsite"  id="companywebsite" value="<?php echo $company_website;?>">
								<small class="text-danger hidden error-flag companywebsite lang-tag" data-lang="alert_company_website">会社のウェブサイトまたはSNSのURL入力してください。</small>
							</div>
						<hr>
						<div class="col12">
							<div class="col-12 form-group lang-tag" data-lang="company_type_section">
								業種(複数回答可)
							</div>
							<div id="company-type-group" class="check-box-group check-box-group-validate">
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-hotel" data-check="hotel_and_accommodation" <?php echo (isset($company_type->hotel_and_accommodation)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="hotel_and_accommodation" for="form-agent">
										ホテル、宿泊施設
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-restaurant" data-check="restaurant_cafe" <?php echo (isset($company_type->restaurant_cafe)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="restaurant_cafe" for="form-wholesale">
										飲食施設 (レストラン、カフェ)
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-transportation" data-check="transportation_service" <?php echo (isset($company_type->transportation_service)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="transportation_service" for="form-land-operation">
										交通サービス
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-tourist" data-check="tourist_attraction" <?php echo (isset($company_type->tourist_attraction)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="tourist_attraction" for="form-land-operation">
										観光施設
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-land-operation" data-check="land_operation" <?php echo (isset($company_type->land_operation)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="land_operation" for="form-land-operation">
										ランドオペレーター
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-city" data-check="city_tourism_bereau" <?php echo (isset($company_type->city_tourism_bereau)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="city_tourism_bereau" for="form-land-operation">
										市観光協会
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-local" data-check="local_goverment" <?php echo (isset($company_type->local_goverment)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="local_goverment" for="form-land-operation">
										地方自治体
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0 input-other" type="checkbox" value="" id="form-other" data-check="other">
									<label class="pl-4 lang-tag" data-lang="other" for="form-other">
										その他
									</label>
									<input type="text" class="form-control2 ml-3 d-inline-block other-validate" placeholder="" name="companyphone"
										id="company-type-other" value="<?php echo $company_type_other;?>" >
								</div>
								<small class="text-danger hidden error-flag company-type-group lang-tag" data-lang="alert_check_one">少なくとも一つ選択してください。</small>
							</div>
						</div>
						<hr>

						<div class="col-12">
							<div class="col-12 form-group lang-tag" data-lang="your_service_grade_jp">
								価格帯/サービスグレード(複数回答可)
							</div>
							 <div id="your-budget" class="check-box-group check-box-group-validate">
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-high" data-check="high" <?php echo (isset($your_budget->high)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="high" for="form-agent">
										高
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-high-middle" data-check="high_middle" <?php echo (isset($your_budget->high_middle)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="high_middle" for="form-wholesale">
										ハイミドル
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-middle" data-check="middle" <?php echo (isset($your_budget->middle)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="middle" for="form-land-operation">
										ミドル
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-middle-low" data-check="middle_low" <?php echo (isset($your_budget->middle_low)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="middle_low" for="form-land-operation">
										中低
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-low" data-check="low" <?php echo (isset($your_budget->low)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="low" for="form-land-operation">
										低
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-non-specific" data-check="non_specific" <?php echo (isset($your_budget->non_specific)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="non_specific" for="form-land-operation">
									   非特定
									</label>
								</div>
								<small class="text-danger hidden error-flag your-budget lang-tag" data-lang="alert_check_one">少なくとも一つ選択してください。</small>
							</div>
						</div>
						<hr>
						<div class="col-12 form-group">
							<label class="lang-tag" for="companydescription" data-lang="company_description">会社の概要について簡潔にご記入ください。</label>
							<textarea class="form-control" id="company-description" rows="3"><?php echo $company_description;?></textarea>
						</div>
						</section>
						<!-- Section1 -->
						<hr>
						<!-- Section2 -->
						<section class="col-12">
							<div class="col-12 form-group">
								<h2 class="form-section-title lang-tag" data-lang="contact_section">担当者様のご連絡先</h2>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="contactname" data-lang="contact_name_label">氏名</label>
								<input type="text" class="form-control lang-tag validate-input" data-lang="contact_name" placeholder=""
									name="contact_name" id="contactname" value="<?php echo $contact_name;?>">
								<small class="text-danger hidden error-flag contactname lang-tag" data-lang="alert_contact_name">担当者様の名前を入力してください。</small>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="contactposition" data-lang="contact_position_label">役職</label>
								<input type="text" class="form-control lang-tag validate-input" data-lang="contact_position" placeholder="" name="contact_position" id="contactposition" value="<?php echo $contact_position;?>">
								<small class="text-danger hidden error-flag contactposition lang-tag" data-lang="alert_contact_position">担当者様の役職を入力してください。</small>
							</div>
							<div class="col-12 form-group">
								<label class="lang-tag" for="contactphone" data-lang="contact_phone">電話番号</label>
								<input type="text" class="form-control validate-input" name="contact_phone"
									id="contactphone" value="<?php echo $contact_phone;?>">
								<small class="text-danger hidden error-flag contactphone lang-tag" data-lang="alert_contact_phone">担当者様の電話番号を入力してください。</small>
							</div>
							<div class="col-12 form-group">
								<label for="contactemail" data-lang="contact_email" class="lang-tag">メールアドレス</label>
								<input type="email" class="form-control validate-input" name="contact_email"
									id="contactemail" value="<?php echo $contact_email;?>">
								<small class="text-danger hidden error-flag contactemail lang-tag" data-lang="alert_contact_email">担当者様のメールアドレスを入力してください。</small>
							</div>
						 </section>
						<!-- Section 2 -->
					</div>
				</div>
             
               
                
                <div class="col-12 bg2">
					<div class="col-12 col-lg-7 center p-0">
						<!-- Section 3 --> 
					   <section class="col-12 section-3">
							<div class="col-12 form-group">
								<h2 class="form-section-title lang-tag" data-lang="company_contact_type_title_jp">タイのどのような企業と商談を行いたいですか？</h2>
							</div>
							<div class="col-12">
								<div class="col-12 form-group lang-tag" data-lang="company_contact_type_jp">
									タイ企業の希望ビジネスタイプ(複数回答可)
								</div>
								<div id="interest-company-group" class="check-box-group check-box-group-validate">
									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="agent_tour" data-check="agent_tour" <?php echo (isset($interest_company->agent_tour)?"checked":"")?>>
										<label class="pl-4 lang-tag" data-lang="agent_tour" for="agent_tour">
											旅行エージェント
										</label>
									</div>
									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="wholesale" data-check="wholesale"  <?php echo (isset($interest_company->wholesale)?"checked":"")?>>
										<label class="pl-4 lang-tag" data-lang="wholesale" for="wholesale">
											旅行卸売
										</label>
									</div>
									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="land_operation" data-check="land_operation" <?php echo (isset($interest_company->land_operation)?"checked":"")?>>
										<label class="pl-4 lang-tag" data-lang="land_operation" for="form-tourist-attraction">
											ランドオペレーター
										</label>
									</div>

									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="other" data-check="other">
										<label class="pl-4 lang-tag" data-lang="other" for="other">
											その他
										</label>
										<input type="text" class="form-control2 ml-3 d-inline-block" placeholder="" name="companyphone" id="interest-company-other" value="<?php echo $interest_company_other;?>">
									</div>
									<small class="text-danger hidden error-flag interest-company-group lang-tag" data-lang="alert_check_one">少なくとも一つ選択してください。</small>
								</div>
							</div>
							<hr>
							<div class="col-12">
								<div class="col-12 form-group lang-tag" data-lang="expect_client_type">
									タイ企業の希望顧客タイプ(複数回答可)
								</div>
								<div id="interest-activity-group" class="check-box-group check-box-group-validate">
									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="form-hotel" data-check="collective" <?php echo (isset($interest_activity->collective)?"checked":"")?>>
										<label class="pl-4 lang-tag" data-lang="collective" for="form-hotel">
											コレクティブ グループ
										</label>
									</div>
									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="form-restaurant" data-check="incentive" <?php echo (isset($interest_activity->incentive)?"checked":"")?>>
										<label class="pl-4 lang-tag" data-lang="incentive"for="form-restaurant">
											インセンティブ グループ
										</label>
									</div>
									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="form-travel" data-check="private" <?php echo (isset($interest_activity->private)?"checked":"")?>>
										<label class="pl-4 lang-tag" data-lang="private" for="form-travel">
											プライベートグループ
										</label>
									</div>
									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="form-attraction" data-check="fit" <?php echo (isset($interest_activity->fit)?"checked":"")?>>
										<label class="pl-4 lang-tag" data-lang="fit" for="form-attraction">
										   個人旅行 (FIT)
										</label>
									</div>
									<div class="col-12 form-check">
										<input class="form-check-input m-0 input-other" type="checkbox" value="" id="form-other" data-check="other">
										<label class="pl-4 lang-tag" data-lang="other" for="form-other">
											その他
										</label>
										<input type="text" class="form-control2 ml-3 d-inline-block other-validate" placeholder="" name="companyphone" id="interest-activity-other" value="<?php echo $interest_activity_other;?>">
									</div>
									<small class="text-danger hidden error-flag interest-activity-group lang-tag" data-lang="alert_check_one">少なくとも一つ選択してください。</small>
								</div>
							</div>
							<hr>
							<div class="col-12">
								<div class="col-12 form-group lang-tag" data-lang="client_budget_group_jp">
									タイ企業の希望顧客予算（複数回答可）
								</div>
								<div id="expect-budget" class="check-box-group check-box-group-validate">
									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="form-collective" data-check="high" <?php echo (isset($expect_budget->high)?"checked":"")?>>
										<label class="pl-4 lang-tag" data-lang="high" for="high">
											高
										</label>
									</div>
									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="high_middle" data-check="high_middle" <?php echo (isset($expect_budget->high_middle)?"checked":"")?>>
										<label class="pl-4 lang-tag" data-lang="high_middle"  for="high_middle">
										  ハイミドル
										</label>
									</div>
									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="form-incentive" data-check="middle" <?php echo (isset($expect_budget->middle)?"checked":"")?>>
										<label class="pl-4 lang-tag" data-lang="middle" for="middle">
										  ミドル
										</label>
									</div>
									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="form-fit" data-check="middle_low" <?php echo (isset($expect_budget->middle_low)?"checked":"")?>>
										<label class="pl-4 lang-tag" data-lang="middle_low" for="middle_low">
										  中低
										</label>
									</div>
									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="form-client-other" data-check="low" <?php echo (isset($expect_budget->low)?"checked":"")?>>
										<label class="pl-4 lang-tag" data-lang="low" for="low">
										   低
										</label>
									</div>
									<div class="col-12 form-check">
										<input class="form-check-input m-0" type="checkbox" value="" id="form-client-other" data-check="non_specific" <?php echo (isset($expect_budget->non_specific)?"checked":"")?>>
										<label class="pl-4 lang-tag" data-lang="non_specific" for="non_specific">
											非特定
										</label>
									</div>
									<small class="text-danger hidden error-flag expect-budget lang-tag" data-lang="alert_check_one">少なくとも一つ選択してください。</small>
								  </div> 
								</div>
					   </section>
							<hr>
						<!-- Section 3 -->  


						<!-- Section 4 -->  
						<section class="col-12 section-4">
							<div class="col-12 form-group">
								<h2 class="form-section-title lang-tag" data-lang="other_request_title" >その他、ご希望がございましたら、ご記入ください。</h2>
							</div>
							<div class="col-12 form-group">
								<textarea class="form-control" id="interestdescription" rows="3"><?php echo $interest_description;?></textarea>
							</div>
							<?php if($company_data):?>
								<div id ="form-validate" class="btn btn-primary mt-5 txt-20 center mb-4 lang-tag" data-lang="edit" >編集</div>
							<?php else:?>
								<div id="form-validate" type="buton" class="btn btn-primary mt-5 txt-20 center mb-4 lang-tag" data-lang="submit" >送る</div>
							<?php endif;?>
						</section>
						<!-- Section 4 --> 
					</div>
				</div>
            </form>
    </section>
    <?php endif;?>
</main>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="col-12 modal-header">
        <button type="button" class="close right mr-3" data-dismiss="modal">&times;</button>
        <h4 class="modal-title center">情報をご確認ください。</h4>
      </div>
      <div class="modal-body">
	  	<div class="container">
			<div class="row">
	  			<div class="col-4 lang-tag" data-lang="company_name">会社名</div><div id="" class="col-8 summary-answer" data-key="company_name"></div>
				<div class="col-4 lang-tag" data-lang="companyphone">電話番号</div><div id="" class="col-8 summary-answer" data-key="company_phone"></div>
				<div class="col-4 lang-tag" data-lang="companyemail">メールアドレス</div><div id="" class="col-8 summary-answer" data-key="company_email"></div>
				<div class="col-4 lang-tag" data-lang="companywebsite">会社のウェブサイト</div><div id="" class="col-8 summary-answer" data-key="company_website"></div>
				<div class="col-4 lang-tag" data-lang="company_type_section">業種</div><div id="" class="col-8 summary-answer" data-key="company_type"></div>
				<div class="col-4 lang-tag" data-lang="your_service_grade_jp">価格帯/サービスグレード</div><div id="" class="col-8 summary-answer" data-key="your_budget"></div>
				<div class="col-4 lang-tag" data-lang="company_description">会社の概要について簡潔にご記入ください。</div><div id="" class="col-8 summary-answer" data-key="company_description"></div>
				<div class="col-4 lang-tag" data-lang="contact_name_label" >氏名</div><div id="" class="col-8 summary-answer" data-key="contact_name"></div>
				<div class="col-4 lang-tag" data-lang="contact_position_label">役職</div><div id="" class="col-8 summary-answer" data-key="contact_position"></div>
				<div class="col-4 lang-tag" data-lang="contact_phone">電話番号</div><div id="" class="col-8 summary-answer" data-key="contact_phone"></div>
				<div class="col-4 lang-tag" data-lang="contact_email">メールアドレス</div><div id="" class="col-8 summary-answer" data-key="contact_email"></div>
				<div class="col-4 lang-tag" data-lang="company_contact_type_jp">タイ企業の希望ビジネスタイプ</div><div id="" class="col-8 summary-answer" data-key="interest_company"></div>
				<div class="col-4 lang-tag" data-lang="expect_client_type">タイ企業の希望顧客タイプ</div><div id="" class="col-8 summary-answer" data-key="interest_activity"></div>
				<div class="col-4 lang-tag" data-lang="client_budget_group_jp">タイ企業の希望顧客予算</div><div id="" class="col-8 summary-answer" data-key="expect_budget"></div>
				<div class="col-4 lang-tag" data-lang="other_request_title" >その他、ご希望がございましたら、ご記入ください。</div><div id="" class="col-8 summary-answer" data-key="interest_description"></div>
			</div>
	  	</div>
      </div>
      <div class="row modal-footer m-3">
	  	<button type="button" class="btn btn-default mt-5 center mb-4 txt-20" data-dismiss="modal">キャンセル</button>
		<?php if($company_data):?>
			<button type="submit" id="form-edit" class="btn btn-primary mt-5 txt-20 center mb-4 lang-tag" data-lang="edit">編集</button>
		<?php else:?>
			<button type="submit" id="form-submit" class="btn btn-primary mt-5 txt-20 center mb-4 lang-tag" data-lang="submit" >送る</button>
		<?php endif;?>
      </div>
    </div>
  </div>
</div>
<?php get_template_part( 'template-parts/cmatch-footer' );?>