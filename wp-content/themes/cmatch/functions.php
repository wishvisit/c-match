<?php

/**
 * cpt functions and definitions
 */

add_filter('intermediate_image_sizes_advanced', 'prefix_remove_default_images');

// Remove default image sizes here. 
function prefix_remove_default_images($sizes)
{
    unset($sizes['small']); // 150px
    unset($sizes['medium']); // 300px
    unset($sizes['large']); // 1024px
    unset($sizes['medium_large']); // 768px
    return $sizes;
}

function theme_support()
{
    add_theme_support('automatic-feed-links');
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(1200, 9999);

    // Custom logo.
    $logo_width  = 120;
    $logo_height = 90;

    // If the retina setting is active, double the recommended width and height.
    if (get_theme_mod('retina_logo', false)) {
        $logo_width  = floor($logo_width * 2);
        $logo_height = floor($logo_height * 2);
    }

    add_theme_support(
        'custom-logo',
        array(
            'height'      => $logo_height,
            'width'       => $logo_width,
            'flex-height' => true,
            'flex-width'  => true,
        )
    );
    add_theme_support('title-tag');
    add_theme_support(
        'html5',
        array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
            'script',
            'style',
        )
    );
    //load_theme_cpt( 'cpt' );
    add_theme_support('align-wide');
    add_theme_support('responsive-embeds');
    // Add theme support for selective refresh for widgets.
    add_theme_support('customize-selective-refresh-widgets');
}

add_action('after_setup_theme', 'theme_support');

function register_styles()
{
    $theme_version = wp_get_theme()->get('Version');
    wp_enqueue_style('cmatch-style', get_stylesheet_uri(), array(), $theme_version);
    wp_style_add_data('cmatch-style', 'rtl', 'replace');
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), null, 'all');
    wp_enqueue_style('cmatch-main', get_template_directory_uri() . '/assets/css/cmatch-main.css', array(), null, 'all');
   
    if (is_page('home')) {
        wp_enqueue_style('cmatch-home', get_template_directory_uri() . '/assets/css/cmatch-home.css', array('bootstrap', 'cmatch-main'), null, 'all');
    }
    if (is_page('register')) {
        wp_enqueue_style('cmatch-home', get_template_directory_uri() . '/assets/css/cmatch-register.css', array('bootstrap', 'cmatch-main'), null, 'all');
    }
    if (is_page('thank-you')) {
        wp_enqueue_style('cmatch-thankyou', get_template_directory_uri() . '/assets/css/cmatch-thankyou.css', array('bootstrap', 'cmatch-main'), null, 'all');
    }
    if (is_page_template('cmatch-register-thai.php')||is_page_template('cmatch-register-japan.php')) {
        wp_enqueue_style('cmatch-eventdetails', get_template_directory_uri() . '/assets/css/cmatch-event-details.css', array('bootstrap', 'cmatch-main'), null, 'all');
    }
    if (is_page_template('cmatch-report.php')) {
        wp_enqueue_style('datatable-css', get_template_directory_uri() . '/assets/js/datatables.min.css', array('bootstrap'), null, 'all');
    }
    if( is_page_template('cmatch-register-jline.php')){
        wp_enqueue_style('tagify-css', get_template_directory_uri() . '/assets/css/tagify.css', array('bootstrap'), null, 'all');
    }
}

add_action('wp_enqueue_scripts', 'register_styles');

function register_scripts()
{

    $theme_version = wp_get_theme()->get('Version');
    wp_enqueue_script('jquery-js', get_template_directory_uri() . '/assets/js/jquery-3.5.1.min.js', array(), $theme_version, true);
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array('jquery-js'), $theme_version, true);
    if (!is_page_template('cmatch-report.php')) {
        wp_enqueue_script('cmatch-custom', get_template_directory_uri() . '/assets/js/cmatch-custom.js', array('jquery-js'), $theme_version, true);
    }
    if (is_page('testing')) {;
        wp_enqueue_script('test-script',  plugins_url('assets/js/test.js', __FILE__), array('jquery',), null, true);
    }
    if (is_page_template('cmatch-register-thai.php' ||is_page_template('cmatch-register-japan.php'))) {
        wp_enqueue_script('firebase-app', 'https://www.gstatic.com/firebasejs/7.15.4/firebase-app.js', '', '');
        wp_enqueue_script('firebase-analytics', 'https://www.gstatic.com/firebasejs/7.15.4/firebase-analytics.js', '', '');
        wp_enqueue_script('firebase-auth', 'https://www.gstatic.com/firebasejs/7.15.4/firebase-auth.js', array('firebase-app'), '');
        wp_enqueue_script('firebase-ui', 'https://cdn.firebase.com/libs/firebaseui/3.5.2/firebaseui.js', array('firebase-app', 'firebase-auth'), '');
        wp_enqueue_script('firebase-message', 'https://www.gstatic.com/firebasejs/7.15.4/firebase-messaging.js', array('firebase-app', 'firebase-auth'), '');
        wp_enqueue_script('firebase-performance', 'https://www.gstatic.com/firebasejs/7.15.4/firebase-performance.js', array('firebase-app', 'firebase-auth'), '');
        wp_enqueue_script('login-script', get_template_directory_uri() . '/assets/js/login-script.js',  array('firebase-app', 'firebase-auth', 'jquery-js'), '');
        wp_localize_script('login-script', 'login_ajax_obj', array('ajax_url' => admin_url('admin-ajax.php')));
    }
    if (is_page_template('cmatch-report.php')) {
        wp_enqueue_script('datatable', get_template_directory_uri() . '/assets/js/datatables.min.js', array('jquery-js'), '',true);
        wp_enqueue_script('datatable-button', get_template_directory_uri() . '/assets/js/Buttons-1.6.3/js/dataTables.buttons.min.js', array('jquery-js','datatable'), '',true);
        wp_enqueue_script('datatable-button-jszip', get_template_directory_uri() . '/assets/js/jszip.min.js', array('jquery-js','datatable','datatable-button'), '',true);
        wp_enqueue_script('datatable-button-html5', get_template_directory_uri() . '/assets/js/Buttons-1.6.3/js/buttons.html5.min.js', array('jquery-js','datatable','datatable-button'), '',true);
        wp_enqueue_script('datatable-custom', get_template_directory_uri() . '/assets/js/datatables-custom.js', array('jquery-js','datatable'), '',true);
    }
    if( is_page_template('cmatch-register-jline.php')){
		wp_enqueue_script( 'tagify-main', get_template_directory_uri() . '/assets/js/tagify.min.js',null, $theme_version, true );
        wp_enqueue_script( 'tagify-custom', get_template_directory_uri() . '/assets/js/tagify-custom.js', array('jquery-js','tagify-main'), $theme_version, true );
        wp_localize_script('tagify-custom', 'tagy_obj', array('ajax_url' => admin_url('admin-ajax.php'),'security'  => wp_create_nonce( 'jline' ),));
    }
}

add_action('wp_enqueue_scripts', 'register_scripts');

function register_menus()
{

    $locations = array(
        'primary'  => __('Desktop Menu', 'cpt'),
        'mobile'   => __('Mobile Menu', 'cpt'),
        'footer'   => __('Footer Menu', 'cpt'),
    );

    register_nav_menus($locations);
}

add_action('init', 'register_menus');


add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

add_action('wp_ajax_nopriv_firebase_ajax_login', 'firebase_ajax_login');
add_action('wp_ajax_firebase_ajax_login', 'firebase_ajax_login');
function firebase_ajax_login()
{ // ajax call
    $status = $msg = '';
    $userdata   = $_POST['userdata'];
    $user_uid 	= $userdata['id'];
    $user_email	= $userdata['email'];
    $user_name	= $userdata['name'];
    $uid	    = $userdata['uid'];
    if (!username_exists($user_uid) && !email_exists($user_email)) {
        // split user and email check for fix facebook login with no email
        //create user
        if (!email_exists($user_email)) {
            $random_password = wp_generate_password($length = 12, $include_standard_special_chars = false);
            $user_id = wp_create_user($user_uid, $random_password, $user_email);
            $user = get_user_by('id', $user_id);
            $user->set_role('subscriber');
            wp_update_user(
                array(
                    'ID' 			=> $user->ID,
                    'first_name' 	=> $user_name,
                    'display_name'	=> $user_name,
                    'email'         => $user_email,
                )
            );
            //add firebase uid
            add_user_meta($user->ID, 'uid', $uid);
        } else {
            $status = 'error';
            $msg    = 'duplicate email';
        }
    } else {
        // or get user by email
        $user = get_user_by('email', $user_email);

        if (!$user) {
            // second attempt for user without email.
            $user = get_user_by('login', $user_uid);
        }
    }

    if ($user) {
        // if user exist, do login.
        wp_set_current_user($user->ID, $user->user_login);
        wp_set_auth_cookie($user->ID);
        do_action('wp_login', $user->user_login);
       
        $event_id   = $_POST['event_id'];
        $is_registerd = is_company_registered($user->ID);
        $is_participant = is_registered_this_event($user->ID,$event_id);
        if($is_participant){
            $status = 'success';
            $msg = "joined";
        }else{
            $status = 'success';
            $msg = 'new';
        }

        
    }else{
        // if not founnd
        $status = 'error';
        $msg    = 'user not found';
    }

    $return = array(
        'status' => $status,
        'msg' => $msg,
    );
    print_r(json_encode($return));
    exit();
}

add_action('wp_ajax_nopriv_register_new_company', 'register_new_company');
add_action('wp_ajax_register_new_company', 'register_new_company');
function register_new_company()
{
    global $wpdb;
    $company_data  			= $_POST['company_data'];
    $this_action            = $company_data['this_action'];
    $company_id 			= $company_data['company_id'];
    $company_country		= $company_data['company_country'];
    $company_name 			= $company_data['company_name'];
    $company_phone			= $company_data['company_phone'];
    $company_email 			= $company_data['company_email'];
    $company_website		= $company_data['company_website'];
    $company_type			= json_encode($company_data['company_type']);
    $company_type_other     = $company_data['company_type_other'];
    $company_customer_type	= json_encode($company_data['company_customer_type']);
    $company_customer_other     = $company_data['company_customer_other'];
    
    $your_budget            = json_encode($company_data['your_budget']);
    $company_description	= $company_data['company_description'];

    $contact_name			= $company_data['contact_name'];
    $contact_position		= $company_data['contact_position'];
    $contact_phone			= $company_data['contact_phone'];
    $contact_email			= $company_data['contact_email'];

    $interest_company		= json_encode($company_data['interest_company']);
    $interest_company_other = $company_data['interest_company_other'];
    $interest_activity		= json_encode($company_data['interest_activity']);
    $interest_activity_other= $company_data['interest_activity_other'];
    $expect_budget          = json_encode($company_data['expect_budget']);
    $interest_description	= $company_data['interest_description'];

    if($this_action !== "form-edit"):
        $wpdb->insert(
            'company_profiles',
            array(
                'id' 					=> '',
                'company_id'            => $company_id,
                'company_country'       => $company_country,        
                'company_name'   		=> $company_name,
                'company_phone'   		=> $company_phone,
                'company_email'   		=> $company_email,
                'company_website'   	=> $company_website,
                'company_type'   		=> $company_type,
                'company_type_other'    => $company_type_other,
                'company_customer_type' => $company_customer_type,
                'company_customer_other'=> $company_customer_other,
                'your_budget'           => $your_budget,
                'company_description'   => $company_description,
                'contact_name'   		=> $contact_name,
                'contact_position'      => $contact_position,
                'contact_phone'   		=> $contact_phone,
                'contact_email'   		=> $contact_email,
                'interest_company'   	=> $interest_company,
                'interest_company_other'=> $interest_company_other,
                'interest_activity'   	=> $interest_activity,
                'interest_activity_other' => $interest_activity_other,
                'expect_budget'         => $expect_budget,
                'interest_description' 	=> $interest_description,
                'remark'   		        => '',
                'created_at'		    => current_time('mysql'),
            ),
            array(
                '%d',
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
            )
        );
        $company_added = $wpdb->insert_id;
        if ($company_added) {
            // Success!
            $mail_status = send_confirm_email($contact_email,$company_country);
            $return = array(
                'status' => 'success',
                'msg' => 'ok',
                'data' => $company_added,
                'mail' => $mail_status,
            );
           
            
            
        } else {
            $return = array(
                'status' => 'error',
                'msg' => "can't save",
                'data' => $company_data,
            );
        }
    else:
        $result = $wpdb->update( 
            'company_profiles', 
            array(       
                'company_name'   		=> $company_name,
                'company_phone'   		=> $company_phone,
                'company_email'   		=> $company_email,
                'company_website'   	=> $company_website,
                'company_type'   		=> $company_type,
                'company_type_other'    => $company_type_other,
                'company_customer_type' => $company_customer_type,
                'company_customer_other'=> $company_customer_other,
                'your_budget'           => $your_budget,
                'company_description'   => $company_description,
                'contact_name'   		=> $contact_name,
                'contact_position'      => $contact_position,
                'contact_phone'   		=> $contact_phone,
                'contact_email'   		=> $contact_email,
                'interest_company'   	=> $interest_company,
                'interest_company_other'=> $interest_company_other,
                'interest_activity'   	=> $interest_activity,
                'interest_activity_other' => $interest_activity_other,
                'expect_budget'         => $expect_budget,
                'interest_description' 	=> $interest_description,
                'remark'   		        => '',
                'created_at'		    => current_time('mysql'),
            ), 
            array( 'company_id' => $company_id ), 
            array( 
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
            ), 
            array( '%d' ) 
        );
        if($result === false){
            $return = array(
                'status' => 'error',
                'msg' => "can't update",
                'data' => $company_data,
            );
        }else{
            $return = array(
                'status' => 'success',
                'msg' => "updated",
                'data' => $company_id,
            );
        }
       
    endif;
    print_r(json_encode($return));
    exit();
}

function is_company_registered($company_id)
{
    global $wpdb;
    $is_registerd_sql   = $wpdb->prepare("SELECT * from company_profiles WHERE company_id = %d",$company_id);
    $is_registerd       = $wpdb->get_row($is_registerd_sql);
    return $is_registerd;
}

function is_registered_this_event($company_id, $event_id){
    global $wpdb;
    $is_registerd_sql   = $wpdb->prepare("SELECT * from company_participant WHERE company_id = %d and event_id = %d",$company_id,$event_id);
    $is_registerd       = $wpdb->get_results($is_registerd_sql,'ARRAY_A');
    return $is_registerd;
}

function send_confirm_email($user_email,$langugage = "TH"){
    if($langugage == "TH"){
        $mail_msg       = file_get_contents(get_template_directory_uri().'/cmatch-thankyou-mail-th.html');
    }else{
        $mail_msg       = file_get_contents(get_template_directory_uri().'/cmatch-thankyou-mail-th.html');
    }
    $headers        = array('Content-Type: text/html; charset=UTF-8','From:C-Match <info@c-match.biz>');
    //$admin_msg    = str_replace($search, $replace, $admin_msg);
    //$mail_sender    = "info@c-match.biz";
    $mail_subject   = "ยืนยันการลงทะเบียน C-match สำเร็จ";
    $sent           = wp_mail($user_email, $mail_subject, $mail_msg,$headers);
    return $sent;
}

function generate_newsletter_email($email,$contact_name,$company_name){
    global $wpdb;
    $sent = 0;
    do{
        if(!$email){
            $return = array(
                'status' => 'error' ,
                'msg' => 'no user email',
                'data' => '',
            );
            return $return;
            break;
        }                    
        // Mail in Thai for customer
        $msg        = file_get_contents(get_template_directory_uri().'/new-letter-th.html');
        $search     = ["***contact_name***","***company_name***"];
        $subject    = "เจรจาธุรกิจกับบริษัทฝั่งญี่ปุ่น ผ่านทางออนไลน์กับ CMatch โดยไม่มีค่าใช้จ่าย";
        $replace    = [$contact_name,$company_name];
        $msg        = str_replace($search, $replace, $msg);
        $headers    = array('Content-Type: text/html; charset=UTF-8','From:CMatch <info@c-match.biz>');
        $sent       = wp_mail($email, $subject, $msg,$headers);
       
    }while(0);

    return $sent;
}
function sent_newsletter_email($status = 0){
    //getlist
    global $wpdb;
    $sql = "SELECT * FROM newsletter_maillist WHERE status = ".$status."  order by id asc limit 5";
    $items = $wpdb->get_results($sql,"ARRAY_A");
    $result = [];
    foreach ($items as $item) {
        $ok = generate_newsletter_email($item['email'],$item['contact_name'],$item['company_name']);
        $result[$item['id']] = "[ok] company :  ".$item['company_name']."\n";
        $update = $wpdb->update(
            'newsletter_maillist',
            array(
                'status' => 2,
            ),
            array(
                'id' => $item['id']
            ),
            array(
                '%d',
            ),
            array(
                '%d'
            )
        );
    }
    print_r($result);
}

function get_report(){
    global $wpdb;
    $sql = "SELECT * FROM company_profiles";
    $items = $wpdb->get_results($sql,"ARRAY_A");
    return $items;
}


add_action('wp_ajax_nopriv_register_jline', 'register_jline');
add_action('wp_ajax_register_jline', 'register_jline');
function register_jline()
{
    if ( ! check_ajax_referer( 'jline', 'security', false ) ) {	
        wp_send_json_error( 'Invalid security token sent.' );	   
        wp_die();	  
    }
    global $wpdb;
    $company_data  			= $_POST['data'];
    $company_data           = json_decode(stripslashes($company_data), true);
    $this_action            = $company_data['this_action'];
    $company_id 			= $company_data['company_id'];
    $company_name 			= $company_data['company_name'];
    $company_type			= $company_data['company_type'];
    $company_description	= $company_data['company_description'];
    $company_email 			= $company_data['company_email'];

    //product
    $product	            = json_encode($company_data['product']);
    $product_category		= json_encode($company_data['product_category']);
    $product_description	= $company_data['product_description'];
    $product_quantity       = $company_data['product_quantity'];
    $product_image          = $company_data['product_image'];
    $product_target         = $company_data['product_target'];

    //contact details
    $contact_name			= $company_data['contact_name'];
    $contact_address		= $company_data['contact_address'];
    $contact_phone			= $company_data['contact_phone'];
    $company_website        = $company_data['company_website'];
    $interest_description	= $company_data['interest_description'];
    $product_img_path = "none";
    if($this_action !== "form-edit"):
        $uploadedfile = $_FILES['file'];
        if($uploadedfile){
            $upload_overrides = array( 'test_form' => false );
            add_filter( 'upload_dir', 'uploadfolder_forproduct' );
                $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
                
            remove_filter( 'upload_dir', 'uploadfolder_forproduct' );
            //var_dump($uploadedfile);
            
            if ( $movefile && ! isset( $movefile['error'] ) ) {
                $product_img_path = $movefile['url'];

                //echo "File is valid, and was successfully uploaded.\n";
                //var_dump( $movefile );
            } else {
                /**
                 * Error generated by _wp_handle_upload()
                 * @see _wp_handle_upload() in wp-admin/includes/file.php
                 */
                //echo $movefile['error'];
                $return = array(
                    'status' => 'error',
                    'msg' => 'ไม่สามารถบันทึกรูปได้',
                    'data' => $movefile['error'],
                );
                print_r(json_encode($return));
                exit();
            }
        }
        $wpdb->insert(
            'company_jline',
            array(
                'id' 					=> '',
                'company_id'            => $company_id,
                'company_name'          => $company_name,
                'company_email'   		=> $company_email,
                'company_type'   	    => $company_type,
                'company_description'   => $company_description,
                "product"               => $product,
                'product_category'      => $product_category,
                'product_description'   => $product_description,
                'product_quantity'      => $product_quantity,
                'product_image'   	    => $product_img_path,
                'product_target'        => $product_target,
                'contact_name'   	    => $contact_name,
                'contact_address'       => $contact_address,
                'contact_phone'         => $contact_phone,
                'company_website'       => $company_website,
                'interest_description'  => $interest_description,
                'remark'   		        => '',
                'created_at'		    => current_time('mysql'),
            ),
            array(
                '%d',
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
            )
        );
        
        $company_added = $wpdb->insert_id;
        if ($company_added) {
            // Success!
            //$mail_status = send_confirm_email($contact_email,$company_country);
            $return = array(
                'status' => 'success',
                'msg' => 'ok',
                'data' => $company_added,
                //'mail' => $mail_status,
            );
           
            
            
        } else {
            $return = array(
                'status' => 'error',
                'msg' => "can't save",
                'data' => $company_data,
            );
        }
    else:
        $result = $wpdb->update( 
            'company_jline', 
            array(       
                'company_name'          => $company_name,
                'company_email'   		=> $company_email,
                'company_type'   	    => $company_type,
                'company_description'   => $company_description,
                "product"   		    => $product,
                'product_category'      => $product_category,
                'product_description'   => $product_description,
                'product_quantity'      => $product_quantity,
                'product_target'        => $product_target,
                'contact_name'   	    => $contact_name,
                'contact_address'       => $contact_address,
                'contact_phone'         => $contact_phone,
                'company_website'       => $company_website,
                'interest_description'  => $interest_description,
            ), 
            array( 'company_id' => $company_id ), 
            array( 
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
            ), 
            array( '%d' ) 
        );
        if($result === false){
            $return = array(
                'status' => 'error',
                'msg' => "can't update",
                'data' => $company_data,
            );
        }else{
            $return = array(
                'status' => 'success',
                'msg' => "updated",
                'data' => $company_id,
            );
        }
       
    endif;
    print_r(json_encode($return));
    exit();
}
function uploadfolder_forproduct( $dirs ) {
    $dirs['subdir'] = '/jline_product';
    $dirs['path'] = $dirs['basedir'] . '/jline_product';
    $dirs['url'] = $dirs['baseurl'] . '/jline_product';
    return $dirs;
}

// mysql table
/*
CREATE TABLE `company_profiles` (
    `id` int(11) NOT NULL,
    `company_id` int(11) NOT NULL,
    `company_name` varchar(200) DEFAULT NULL,
    `company_phone` varchar(30) DEFAULT NULL,
    `company_email` varchar(100) DEFAULT NULL,
    `company_website` varchar(255) DEFAULT NULL,
    `company_type` json DEFAULT NULL COMMENT 'json',
    `company_customer_type` json DEFAULT NULL COMMENT 'json',
    `company_description` varchar(255) DEFAULT NULL,
    `contact_name` varchar(100) DEFAULT NULL,
    `contact_position` varchar(50) DEFAULT NULL,
    `contact_phone` varchar(30) DEFAULT NULL,
    `contact_email` varchar(100) DEFAULT NULL,
    `interest_company` json DEFAULT NULL,
    `interest_activity` json DEFAULT NULL,
    `interest_description` varchar(255) DEFAULT NULL,
    `remark` varchar(255) DEFAULT NULL,
    `created_at` datetime DEFAULT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
  

  ALTER TABLE `company_profiles`
    ADD KEY `id` (`id`);
  
  --
  -- AUTO_INCREMENT for dumped tables
  --
  
  --
  -- AUTO_INCREMENT for table `company_profiles`
  --
  ALTER TABLE `company_profiles`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
    
    
    CREATE TABLE `company_participant` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `company_participant`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company_participant`
--
ALTER TABLE `company_participant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;COMMIT;
*/