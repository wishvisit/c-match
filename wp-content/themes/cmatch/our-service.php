<?php
/**
* Template Name: Our service
 *
 */

get_header();
?>

<main id="main-container" class="site-content container" role="main">
    <div class="row">
        <h2 id="side-service-header" class="d-block d-md-none mobile-title">Service list</h2>
        <div id="our-service-moboile" class="d-block d-md-none mobile-button">
                <span class="service-count">0</span><span class="service-count-bg"></span>
            </div>
        <div class="col-12 col-md-3 service-list">
            <h2 id="side-service-header" class="d-none d-xs-none d-sm-none d-md-block">Service list</h2>
           
            <ul id="service-list">
            <?php 
                 $args = array(  
                    'post_type' => 'service',
                    'post_status' => 'publish',
                    'orderby' => 'parent', 
                    'order' => 'ASC',
                    'cat' => 'home',
                );
            
                $loop = new WP_Query( $args ); 
                    
                while ( $loop->have_posts() ) : $loop->the_post();
                    //$featured_img = wp_get_attachment_image_src( $post->ID );?>
                    <li class="service-item" data-post-id="<?php echo $post->ID;?>" data-service-name="<?php echo the_title();?>"><?php echo the_title();?></li>
                <?php endwhile;
            
                wp_reset_postdata(); 
            ?>
            </ul>   
        </div>
        <div class="col-12 col-md-3 selected-service hidden">
            <h4 class="d-none d-xs-none d-sm-none d-md-block selected-service-header">Selected list</h4>
            <span class="service-count">0</span><span class="service-count-bg"></span>
            <div class="">
                <ul class="selected-list-output">
                </ul>
            </div>
            <div class="interest-more"></div>
            <div class="interest-send"></div>
        </div>
        <div class="col-12 col-md-9 service-content">
            <div id="content-output">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/dummy-slide.jpg" alt="slide"/>
            </div>
            <div id="interest-service" data-service-id="85" data-service-name="Accounting &amp; Document" >

            </div>
        </div>
    </div>
</main><!-- #site-content -->
<div id="how-to-modal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      <div id="how-to-slide" class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#how-to-slide" data-slide-to="0" class="active"></li>
            <li data-target="#how-to-slide" data-slide-to="1"></li>
            <li data-target="#how-to-slide" data-slide-to="2"></li>
            <li data-target="#how-to-slide" data-slide-to="3"></li>
            <li data-target="#how-to-slide" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="<?php echo get_template_directory_uri()?>/assets/images/how-to-ourservice1.png" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100"src="<?php echo get_template_directory_uri()?>/assets/images/how-to-ourservice2.png" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo get_template_directory_uri()?>/assets/images/how-to-ourservice3.png" alt="Third slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo get_template_directory_uri()?>/assets/images/how-to-ourservice4.png" alt="Third slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo get_template_directory_uri()?>/assets/images/how-to-ourservice5.png" alt="Third slide"> 
                <button type="button" class="btn btn-secondary dismiss" data-dismiss="modal">Close</button>
            </div>
        </div>
        <a class="carousel-control-prev" href="#how-to-slide" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#how-to-slide" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
get_footer();