<?php
/**
* Template Name: Register Thai
 *
 */
get_template_part( 'template-parts/cmatch-header' );?>
<main id="main-container" class="home-page-container form-thai">
    <?php get_template_part( 'template-parts/cmatch-menu' );?>
    <!--<section id="register-page-title" class="full-page-background">
         <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="event-title">
                      
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/dummy-thankyou.png" alt="dummy bg" />
                        <?php /* change by script
                        <p class="font-prompt">Welcome to</p>
                        <h2 class="font-prompt title">SAKURA NO SATO</p>
                        <p class="font-prompt sub-title">Event</p> */?>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <section id="register-bar">
        <div class="container">
           <div class="col-12 col-lg-8 center">
				<div class="row justify-content-md-center">
					<div class="col col-lg-3">
						<img class="thaiflag" src="<?php echo get_template_directory_uri();?>/assets/images/thai-flag.png" alt="Thai Flag" />
					</div>
					<div class="col col-lg-3">
						<img id="step-register" class="step-logo active" src="<?php echo get_template_directory_uri();?>/assets/images/register/C Match-41-100white.png" alt="Thai Flag" />
					</div>
					<div class="col col-lg-3">
						<img id="step-select" class="step-logo" src="<?php echo get_template_directory_uri();?>/assets/images/register/C Match-45.png" alt="Thai Flag" />
					</div>
					<div class="col col-lg-3">
						<img id="step-date" class="step-logo" src="<?php echo get_template_directory_uri();?>/assets/images/register/C Match-46.png" alt="Thai Flag" />
					</div>
				</div>
            </div>
        </div>
    </section>
    <?php if(!is_user_logged_in()):?>
      <div class="container p-0" style=" -moz-box-shadow: 0px 5px 200px #c8f0ff;
  -webkit-box-shadow: 0px 5px 200px #c8f0ff;
  box-shadow: 0px 5px 200px #c8f0ff;">
    	<div class="">
    		<img src="<?php echo get_template_directory_uri();?>/assets/images/register/CMatch-banner-09.png">
    		<div class="row p-5">
    			<div class="col-12 col-lg-5">
    				<img src="<?php echo get_template_directory_uri();?>/assets/images/register/CMatch-banner-10.png">
    				<div class="row">
    					<div class="col-12"><img src="<?php echo get_template_directory_uri();?>/assets/images/register/CMatch-banner-11.png"></div>
    				</div>
    			</div>
    			<div class="col-12 col-lg-7">
					<section id="register-login">
						<h2 class="login-notice text-center lang-tag" data-lang="before_login" >ลงทะเบียนที่นี่</h2>
						<link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/3.5.2/firebaseui.css" />
						<div id="firebaseui-auth-container"></div>
						<input type="hidden" id="event-id" value="<?php echo get_the_ID();?>">
					</section>
   				</div>
    		</div>
    		
    	</div>
    
		
    </div>
    <?php else:?>
        <?php if($current_user){
            $company_id = $current_user->ID;
            $company_country = "TH";
            $company_data = is_company_registered($current_user->ID);
            $company_name = $company_phone = $company_email = $company_description = $company_website = $contact_position = $contact_phone= $contact_email= $interest_description= $contact_name = $remark = $company_type_other = $company_customer_other = $interest_company_other = $interest_activity_other ="";
            if($company_data){
                $company_name           = $company_data->company_name;
                $company_phone          = $company_data->company_phone;
                $company_email          = $company_data->company_email;
				$company_description    = $company_data->company_description;
				$your_budget			= json_decode($company_data->your_budget);
				$company_type			= json_decode($company_data->company_type);
				$company_type_other		= $company_data->company_type_other;
				$company_customer_type	= json_decode($company_data->company_customer_type);
				$company_customer_other = $company_data->company_customer_other;
                $company_website        = $company_data->company_website;
                $contact_name           = $company_data->contact_name;
                $contact_position       = $company_data->contact_position;
                $contact_phone          = $company_data->contact_phone;
				$contact_email          = $company_data->contact_email;
				$interest_company		= json_decode($company_data->interest_company);
				$interest_company_other = $company_data->interest_company_other;
				$interest_activity		= json_decode($company_data->interest_activity);
				$interest_activity_other= $company_data->interest_activity_other;
				$expect_budget			= json_decode($company_data->expect_budget);
                $interest_description   = $company_data->interest_description;
				$remark                 = $company_data->remark;
				if($company_data->company_country !== $company_country && isset($company_data->company_country) ){
					wp_redirect("/japan-company");
					exit();
				}else{
					$showalert = "hidden";
				}
			}			
		
        }?>
    <section id="home-title" class="full-page-background register-detail">
       
            <form class="col-12 center" id="register-form">
              <div class="col-12 prompt-regular gray1">
              <!-- bg 1 -->
              <div class="col-12 col-lg-7 center bg1">
               <section class="col-12 section-1">
               		<div class="col-12 q1 p-0">
						<div class="col-12 form-group ">
							<h2 class="form-section-title prompt-semi lang-tag" data-lang="form_company_data_title">ข้อมูลบริษัทของท่าน</h2>
							<input type="hidden" id="companyid" value="<?php echo $company_id;?>">
							<input type="hidden" id="companycountry" value="<?php echo $company_country;?>">
						</div>
						<div class="col-12 form-group">
							<label class="lang-tag" for="companynames" data-lang="company_name">ชื่อบริษัท</label>
							<input type="text" class="form-control lang-tag validate-input" data-lang="company_name" 
								name="companyname" id="companyname" value="<?php echo $company_name;?>">
							<small class="text-danger hidden error-flag companyname">กรุณาระบุชื่อบริษัท</small>
						</div>
						<div class="col-12 form-group">
							<label class="lang-tag" for="companyphone" data-lang="companyphone">เบอร์โทรศัพท์</label>
							<input type="text" class="form-control validate-input"  name="companyphone"
								id="companyphone" value="<?php echo $company_phone;?>">
							<small class="text-danger hidden error-flag companyphone">กรุณาระบุเบอร์โทรศัพท์บริษัท</small>
						</div>
						<div class="col-12 form-group">
							<label for="companyemail" data-lang="companyemail" class="lang-tag">อีเมลกลางของบริษัท</label>
							<input type="email" class="form-control validate-input" name="companyemail"
								id="companyemail" value="<?php echo $company_email;?>">
							<small class="text-danger hidden error-flag companyemail">กรุณาระบุอีเมลบริษัท</small>
						</div>
						<div class="col-12 form-group">
							<label class="lang-tag" for="companywebsite" data-lang="companywebsite">เว็บไซต์บริษัท</label>
							<input type="text" class="form-control validate-input" name="companywebsite"  id="companywebsite" value="<?php echo $company_website;?>">
							<small class="text-danger hidden error-flag companywebsite">กรุณาระบุlink เว็บไซต์บริษัท หรือ link page social media</small>
						</div>
                	</div>
                
                <hr>
                <div class="col-12 q2 p-0">
					<div class="col-12 form-group lang-tag" data-lang="company_type_section">
						ประเภทบริษัทของท่าน (ตอบได้มากกว่า 1 ข้อ)
					</div>
					<div id="company-type-group" class="check-box-group check-box-group-validate">
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-agent" data-check="agent_tour" <?php echo (isset($company_type->agent_tour)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="agent_tour" for="form-agent">
								Agent tour
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-wholesale" data-check="wholesale" <?php echo (isset($company_type->collective)?"wholesale":"")?>>
							<label class="pl-4 lang-tag" data-lang="wholesale"  for="form-wholesale">
								Wholesale
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-land-operation" data-check="land_operation" <?php echo (isset($company_type->land_operation)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="land_operation"  for="form-land-operation">
								Land Operation
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0 input-other" type="checkbox" value="" id="form-other" data-check="other">
							<label class="pl-4 lang-tag" data-lang="other" for="form-other">
								อื่นๆ
							</label>
							<input type="text" class="form-control2 ml-3 d-inline-block other-validate" placeholder="" name="companyphone"
								id="company-type-other" value="<?php echo $company_type_other;?>" >
						</div>
						<small class="text-danger hidden error-flag company-type-group">กรุณาเลือกอย่างน้อย 1 ตัวเลือก</small>
					</div>
                </div>
                <hr>
                <div class="col-12 q3 p-0">
					<div class="col-12 form-group lang-tag" data-lang="company_client_type">
						ประเภทลูกค้า (ตอบได้มากกว่า 1 ข้อ)
					</div>
					<div id="company-customer-group" class="check-box-group check-box-group-validate">
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-collective" data-check="collective" <?php echo (isset($company_customer_type->collective)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="collective"  for="form-collective">
								Collective group
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-joint-tour" data-check="incentive" <?php echo (isset($company_customer_type->incentive)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="incentive"  for="form-joint-tour">
								Incentive group
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-incentive" data-check="private" <?php echo (isset($company_customer_type->private)?"checked":"")?>>
							<label class="pl-4 vlang-tag" data-lang="private" for="form-incentive">
								Private group
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-fit" data-check="fit" <?php echo (isset($company_customer_type->fit)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="fit" for="form-fit">
								FIT
							</label>
						</div>
						<div class="col-12 form-check ">
							<input class="form-check-input m-0 input-other" type="checkbox" value="" id="form-client-other" data-check="other">
							<label class="pl-4 lang-tag" data-lang="other"  for="form-client-other">
								อื่นๆ
							</label>
							<input type="text" class="form-control2 d-inline-block ml-3 other-validate" placeholder="" name="orm-client-other"
								id="company-customer-other" value="<?php echo $company_customer_other;?>">
						</div>
						<small class="text-danger hidden error-flag company-customer-group">กรุณาเลือกอย่างน้อย 1 ตัวเลือก</small>
					</div>
                </div>
                <hr>
                <div class="col-12 q4 p-0">
					<div class="col-12 form-group lang-tag" data-lang="client_budget_group_th">
						ระดับงบประมาณของลูกค้า (ตอบได้มากกว่า 1 ข้อ)
					</div>
					<div id="your-budget" class="check-box-group check-box-group-validate">
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-collective" data-check="high" <?php echo (isset($your_budget->high)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="high" for="form-collective">
								สูง
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-joint-tour" data-check="high_middle" <?php echo (isset($your_budget->high_middle)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="high_middle" for="form-joint-tour">
							  กลางค่อนสูง
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-incentive" data-check="middle" <?php echo (isset($your_budget->middle)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="middle" for="form-incentive">
							  กลาง
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-fit" data-check="middle_low" <?php echo (isset($your_budget->middle_low)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="middle_low" for="form-fit">
							  กลางค่อนต่ำ
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-client-other" data-check="low" <?php echo (isset($your_budgeg->low) ?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="low" for="form-client-other">
							   ต่ำ
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-client-other" data-check="non_specific">
							<label class="pl-4 lang-tag" data-lang="non_specific" for="form-client-other">
								ไม่กำหนด
							</label>
						</div>	
						<small class="text-danger hidden error-flag your-budget">กรุณาเลือกอย่างน้อย 1 ตัวเลือก</small>
						<hr>
					</div>
                </div>
						<div class="col-12 form-group q5">
							<label class="lang-tag" for="companydescription" data-lang="company_description">อธิบายเกี่ยวกับบริษัทสั้นๆ</label>
							<textarea class="form-control" id="company-description" rows="3"><?php echo $company_description;?></textarea>
						</div>
                </section>
                <!-- Section-01 -->
                
                <hr>
                <!-- Section-02 -->
                <section class="col-12 section-2">
					<div class="col-12 form-group q6 p-0">
						<h2 class="form-section-title lang-tag" data-lang="contact_section" >ข้อมูลผู้ติดต่อ</h2>
					</div>
					<div class="col-12 form-group">
						<label class="lang-tag" for="contactname" data-lang="contact_name_label">ชื่อ</label>
						<input type="text" class="form-control lang-tag validate-input" data-lang="contact_name" placeholder=""
							name="contact_name" id="contactname" value="<?php echo $contact_name;?>">
						<small class="text-danger hidden error-flag contactname">กรุณาระบุขื่อผู้ติดต่อ</small>
					</div>
					<div class="col-12 form-group">
						<label class="lang-tag" for="contactposition" data-lang="contact_position_label">ตำแหน่ง</label>
						<input type="text" class="form-control lang-tag validate-input" data-lang="contact_position" placeholder=""
							name="contact_position" id="contactposition" value="<?php echo $contact_position;?>">
						<small class="text-danger hidden error-flag contactposition">กรุณาระบุตำแหน่งงานของผู้ติดต่อ</small>
					</div>
					<div class="col-12 form-group">
						<label class="lang-tag" for="contactphone" data-lang="contact_phone">เบอร์โทรศัพท์</label>
						<input type="text" class="form-control validate-input" name="contact_phone"
							id="contactphone" value="<?php echo $contact_phone;?>">
						<small class="text-danger hidden error-flag contactphone">กรุณาระบุเบอร์โทรศัพท์ของผู้ติดต่อ</small>
					</div>
					<div class="col-12 form-group">
						<label for="contactemail" data-lang="contact_email" class="lang-tag">อีเมล</label>
						<input type="email" class="form-control validate-input" name="contact_email"
							id="contactemail" value="<?php echo $contact_email;?>">
						<small class="text-danger hidden error-flag contactemail">กรุณาระบุอีเมลของผู้ติดต่อ</small>
					</div>
				</section>
             <!-- Section 2 -->
				</div>				
				</div>
                <!-- bg 1 -->
              <div class="col-12 bg2">
               <div class="col-12 col-lg-7 center prompt-regular gray1">
        	<!-- Section 3 --> 
               	<section class="col-12 sectiom-3">
               		<div class="col-12 q7 p-0">
						<div class="col-12 form-group">
							<h2 class="form-section-title lang-tag"  data-lang="company_contact_type_title_th">ข้อมูลบริษัทฝั่งญี่ปุ่นที่อยากเจรจาธุรกิจด้วย</h2>
						</div>
						<div class="col-12 p-0">
							<div class="col-12 form-group lang-tag" data-lang="company_contact_type_th">
								ประเภทบริษัทฝั่งญี่ปุ่น  (ตอบได้มากกว่า 1 ข้อ)
							</div>
							<div id="interest-company-group" class="check-box-group check-box-group-validate">
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-hotel-accommodation" data-check="hotel_and_accommodation" <?php echo (isset($interest_company->hotel_and_accommodation)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="hotel_and_accommodation" for="form-hotel_accommodation">
										Hotel, Accommodation
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-restaurant-cafe" data-check="restaurant_cafe" <?php echo (isset($interest_company->restaurant_cafe)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="restaurant_cafe" for="form-restaurant-cafe">
										Restaurant, Cafe
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-transportation" data-check="transportation_service" <?php echo (isset($interest_company->transportation_service)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="transportation_service" for="form-transportation">
										Transportation Service
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-tourist-attraction" data-check="tourist_attraction" <?php echo (isset($interest_company->tourist_attraction)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="tourist_attraction" for="form-tourist-attraction">
										Tourist Attraction
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-land-operation" data-check="land_operation" <?php echo (isset($interest_company->land_operation)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="land_operation" for="form-land-operation">
										Land Operation
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-city-tourism-bereau" data-check="city_tourism_bereau" <?php echo (isset($interest_company->city_tourism_bereau)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="city_tourism_bereau" for="form-city-tourism-bereau">
										City Tourism Bureau
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0" type="checkbox" value="" id="form-local-goverment" data-check="local_goverment" <?php echo (isset($interest_company->local_goverment)?"checked":"")?>>
									<label class="pl-4 lang-tag" data-lang="local_goverment" for="form-local-goverment">
										Local government
									</label>
								</div>
								<div class="col-12 form-check">
									<input class="form-check-input m-0 input-other" type="checkbox" value="" id="form-other">
									<label class="pl-4 lang-tag" data-lang="other" for="form-other">
										อื่นๆ
									</label>
									<input type="text" class="form-control2 ml-3 d-inline-block d-inline-block other-validate" placeholder="" name="companyphone" id="interest-company-other" value="<?php echo $interest_company_other;?>">
								</div>
								<small class="text-danger hidden error-flag interest-company-group">กรุณาเลือกอย่างน้อย 1 ตัวเลือก</small>
							</div>
						</div>
					</div>
                <hr>
                <div class="col-12 q8 p-0">
					<div class="col-12 form-group lang-tag" data-lang="interest_section_title">
						สนใจข้อมูลเกี่ยวกับอะไร (ตอบได้มากกว่า 1 ข้อ)
					</div>
					<div id="interest-activity-group" class="check-box-group check-box-group-validate">
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-hotel" data-check="hotel_and_accommodation" <?php echo (isset($interest_activity->hotel_and_accommodation)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="hotel_and_accommodation"for="form-hotel">
								โรงแรม ที่พัก
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-restaurant" data-check="restaurant" <?php echo (isset($interest_activity->restaurant)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="restaurant"for="form-restaurant">
								ร้านอาหาร
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-travel" data-check="transportation" <?php echo (isset($interest_activity->transportation)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="transportation" for="form-travel">
								การเดินทาง
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-attraction" data-check="tourist_attractions" <?php echo (isset($interest_activity->tourist_attractions)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="tourist_attractions" for="form-attraction">
								สถานที่ท่องเที่ยว
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0 input-other" type="checkbox" value="" id="form-other" data-check="other">
							<label class="pl-4 d-inline-block lang-tag" data-lang="other" for="form-other">
								อื่นๆ
							</label>
							<input type="text" class="form-control2 ml-3 d-inline-block other-validate" placeholder="" name="companyphone" id="interest-activity-other" value="<?php echo $interest_activity_other;?>">
						</div>
						<small class="text-danger hidden error-flag interest-activity-group">กรุณาเลือกอย่างน้อย 1 ตัวเลือก</small>
					</div>
               </div>
                <hr>
                <div class="col-12 q9 p-0">
					<div class="col-12 form-group lang-tag" data-lang="expect_service_price_title_th">
						ระดับราคา/การให้บริการของบริษัทที่ท่านอยากติดต่อ (ตอบได้มากกว่า 1 ข้อ)
					</div>
					<div id="expect-budget" class="check-box-group check-box-group-validate">
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-collective" data-check="high" <?php echo (isset($expect_budget->high)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="high" for="form-collective">
								สูง
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-joint-tour" data-check="high_middle" <?php echo (isset($expect_budget->high_middle)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="high_middle"for="form-joint-tour">
							  กลางค่อนสูง
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-incentive" data-check="middle" <?php echo (isset($expect_budget->middle)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="middle" for="form-incentive">
							  กลาง
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-fit" data-check="middle_low" <?php echo (isset($expect_budget->middle_low)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="middle_low" for="form-fit">
							  กลางค่อนต่ำ
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-client-other" data-check="low" <?php echo (isset($expect_budget->low)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="low" for="form-client-other">
							   ต่ำ
							</label>
						</div>
						<div class="col-12 form-check">
							<input class="form-check-input m-0" type="checkbox" value="" id="form-client-other" data-check="non_specific" <?php echo (isset($expect_budget->non_specific)?"checked":"")?>>
							<label class="pl-4 lang-tag" data-lang="non_specific" for="form-client-other">
								ไม่กำหนด
							</label>
						</div>
						<small class="text-danger hidden error-flag expect-budget">กรุณาเลือกอย่างน้อย 1 ตัวเลือก</small>
					  </div> 
                   </div>
					</section>
                    
                <!-- Section 3 -->  
                <hr>
                
                <!-- Section 4 -->
                <section class="col-12 section-4">
					<div class="col-12 form-group q10">
						<h2 class="form-section-title lang-tag" data-lang="other_request_title" >ความต้องการอื่นๆ (special request)</h2>
					</div>
					<div class="col-12 form-group">
						<textarea class="form-control" id="interestdescription" rows="3"><?php echo $interest_description;?></textarea>
					</div>
					<?php if($company_data):?>
						<div id="form-validate"class="btn btn-primary mt-5 txt-20 center mb-4 lang-tag" data-lang="edit">แก้ไข</div>
					<?php else:?>
						<div id="form-validate"  class="btn btn-primary mt-5 txt-20 center mb-4 lang-tag" data-lang="submit"  >ยืนยัน</div>
					<?php endif;?>
                </section>  
                <!-- Section 4 --> 
				  </div>
				</div>
            </form>
    </section>
    <?php endif;?>
</main>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="col-12 modal-header">
        <button type="button" class="close right mr-3" data-dismiss="modal">&times;</button>
        <h4 class="modal-title center">ตรวจสอบข้อมูล</h4>
      </div>
      <div class="modal-body">
        <div class="container">
			<div class="row">
				<div class="col-4 lang-tag" data-lang="company_name">ชื่อบริษัท</div><div id="" class="col-8 summary-answer" data-key="company_name"></div>
				<div class="col-4 lang-tag" data-lang="companyphone">เบอร์โทรศัพท์</div><div id="" class="col-8 summary-answer" data-key="company_phone"></div>
				<div class="col-4 lang-tag" data-lang="companyemail">อีเมลกลางของบริษัท</div><div id="" class="col-8 summary-answer" data-key="company_email"></div>
				<div class="col-4 lang-tag" data-lang="companywebsite">เว็บไซต์บริษัท</div><div id="" class="col-8 summary-answer" data-key="company_website"></div>
				<div class="col-4 lang-tag" data-lang="company_type_section">ประเภทบริษัทของท่าน</div><div id="" class="col-8 summary-answer" data-key="company_type"></div>
				<div class="col-4 lang-tag" data-lang="company_client_type">ประเภทลูกค้า</div><div id="" class="col-8 summary-answer" data-key="company_customer_type"></div>
				<div class="col-4 lang-tag" data-lang="client_budget_group_th">ระดับงบประมาณของลูกค้า</div><div id="" class="col-8 summary-answer" data-key="your_budget"></div>
				<div class="col-4 lang-tag" data-lang="company_description">อธิบายเกี่ยวกับบริษัทสั้นๆ</div><div id="" class="col-8 summary-answer" data-key="company_description"></div>
				<div class="col-4 lang-tag" data-lang="contact_section" >ข้อมูลผู้ติดต่อ</div><div id="" class="col-8 summary-answer" data-key="contact_name"></div>
				<div class="col-4 lang-tag" data-lang="contact_position_label">ตำแหน่ง</div><div id="" class="col-8 summary-answer" data-key="contact_position"></div>
				<div class="col-4 lang-tag" data-lang="contact_phone">เบอร์โทรศัพท์</div><div id="" class="col-8 summary-answer" data-key="contact_phone"></div>
				<div class="col-4 lang-tag" data-lang="contact_email">อีเมล</div><div id="" class="col-8 summary-answer" data-key="contact_email"></div>
				<div class="col-4 lang-tag" data-lang="company_contact_type_th">ประเภทบริษัทฝั่งญี่ปุ่น</div><div id="" class="col-8 summary-answer" data-key="interest_company"></div>
				<div class="col-4 lang-tag" data-lang="interest_section_title">สนใจข้อมูลเกี่ยวกับอะไร</div><div id="" class="col-8 summary-answer" data-key="interest_activity"></div>
				<div class="col-4 lang-tag" data-lang="expect_service_price_title_th">ระดับราคา/การให้บริการของบริษัทที่ท่านอยากติดต่อ</div><div id="" class="col-8 summary-answer" data-key="expect_budget"></div>
				<div class="col-4 lang-tag" data-lang="other_request_title" >ความต้องการอื่นๆ</div><div id="" class="col-8 summary-answer" data-key="interest_description"></div>
			</div>
		</div>
      </div>
      <div class="row modal-footer m-3">
        <button type="button" class="btn btn-default mt-5 center mb-4 txt-20" data-dismiss="modal">ยกเลิก</button>
		<?php if($company_data):?>
		<button type="submit" id="form-edit" class="btn btn-primary mt-5 txt-20 center mb-4 lang-tag" data-lang="edit">แก้ไข</button>
	<?php else:?>
		<button type="submit" id="form-submit" class="btn btn-primary mt-5 txt-20 center mb-4 lang-tag" data-lang="submit" >ยืนยัน</button>
	<?php endif;?>
      </div>
    </div>

  </div>
</div>
<?php get_template_part( 'template-parts/cmatch-footer' );?>