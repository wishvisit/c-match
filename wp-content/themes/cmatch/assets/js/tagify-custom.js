var productElm = document.querySelector('input[name=product]'),
    productList = ["Anti-bacteria Spray & Soap","Bag & Dress","Bag & Luggage","Body Cream & Scrub","Calcium & Protein Capsule","Cloth","Coconut Drink","Coconut Jelly Drink","Coconut Milk","Coconut Oil","Coffee (Doi Chang)","Collagen","Collagen Soap","Cosmetics & Food Supplement","Draft Beer","Dried Durian","Dried Fruit","Dried Fruit ","Dried Fruit & Coffee Bean","Ethanol Oil & Dried Durian","Face Cream","Face Mask","Food Supplement","Fragrant Wood","Fried Durian","Frozen Coconut & Malberry Drink","Frozen Food, Germany Pork, BBQ, Fried Salmon Skin","Fruit Jelly","Gluta Collagen","Handmade Bag","Handmade Local Hat","Handmade Local Textile & Garment","Handmade Plant Pot","Handmade Silk & Sculpture","Herb Scrub & Face Mask","Herb Tea","Instant Thai Meal","Laundry Powder","Lingerie","Lotion & Body Cream","Low Calories Beverage","Maternity Supplement","Meal Bar","Milk Snack","Muffler","Nautical Skincare","Personal Care Product","Pickled ginger","Pocket Purse for Men","Popcorn Coconut","Processed Food","Rice","Rice Capsule & Powder","Sanitary Mask (Colorful)","Sanitizer Spray","Sauce & Snack","Shrimp & Fish Ball","Silver Accessories","Skincare","Snack","Solid Perfume","Spa Product & Scrub","Spa Soap","Spray for Foot","Swimsuit","Tea (Cha Tra Mue)","Thai Pepper","Thai Silk","Thai Snack","Thongmuan Thai Snack","Tooth Spray for Kids","Topping Sauces","Toy ","Vegetable Powder","Vitamin C Serum","Wall Hanger","Weight Supplement"],
    productCategory = ["Baby/Children Product","Beauty & Cosmetics","Books","Clothing ","Electronics & IT","Etc","Fashion & Apparel & Accessories","Grocery/Food/Beverage","Health & Personal Care","Houseware/Kitchenware/Furniture","Office Supplies","Pet","Plant","Plant ","Spa & Relaxation","Sport & Fitness","Toys & Games","Travel","Others"];

// initialize Tagify on the above input node reference
var tagProduct = new Tagify(productElm, {
    //enforceWhitelist: true,
    placeholder : 'ผลิตภัณฑ์',
    whitelist : productList,
    //whitelist: productElm.value.trim().split(/\s*,\s*/), // Array of values. stackoverflow.com/a/43375571/104380
    dropdown: {
        maxItems: 20,           // <- mixumum allowed rendered suggestions
        classname: "list", // <- custom classname for this dropdown, so it could be targeted
        enabled: 0,             // <- show suggestions on focus
        closeOnSelect: true    // <- do not hide the suggestions dropdown once an item has been selected
      }
});
var productCategoryElm = document.querySelector('input[name=product_category]');
var tagProductCategory = new Tagify(productCategoryElm, {
    //enforceWhitelist: true,
    placeholder : 'หมวดผลิตภัณฑ์',
    whitelist : productCategory,
    //whitelist: productElm.value.trim().split(/\s*,\s*/), // Array of values. stackoverflow.com/a/43375571/104380
    dropdown: {
        maxItems: 20,           // <- mixumum allowed rendered suggestions
        classname: "list", // <- custom classname for this dropdown, so it could be targeted
        enabled: 0,             // <- show suggestions on focus
        closeOnSelect: true    // <- do not hide the suggestions dropdown once an item has been selected
      }
})
// Chainable event listeners
tagProduct
      //.on('add', onAddTag)
      //.on('remove', onRemoveTag)
      .on('input', onInput)
      //.on('edit', onTagEdit)
      //.on('invalid', onInvalidTag)
      //.on('click', onTagClick)
      //.on('focus', onTagifyFocusBlur)
      //.on('blur', onTagifyFocusBlur)
      //.on('dropdown:hide dropdown:show', e => console.log(e.type))
      //.on('dropdown:select', onDropdownSelect)

var mockAjax = (function mockAjax(){
    var timeout;
    return function(duration){
        clearTimeout(timeout); // abort last request
        return new Promise(function(resolve, reject){
            timeout = setTimeout(resolve, duration || 700, productList)
        })
    }
})()

// tag added callback
function onAddTag(e){
    console.log("onAddTag: ", e.detail);
    console.log("original input value: ", productElm.value)
    tagProduct.off('add', onAddTag) // exmaple of removing a custom Tagify event
}

// on character(s) added/removed (user is typing/deleting)
function onInput(e){
    //console.log("onInput: ", e.detail);
    // tagProduct.settings.whitelist.length = 0; // reset current whitelist
    //tagProduct.loading(true) // show the loader animation

    // get new whitelist from a delayed mocked request (Promise)
    // mockAjax()
    //     .then(function(result){
    //         // replace tagify "whitelist" array values with new values
    //         // and add back the ones already choses as Tags
    //         tagProduct.settings.whitelist.push(...result, ...tagProduct.value)

    //         tagProduct
    //           .loading(false)
    //           // render the suggestions dropdown.
    //           .dropdown.show.call(tagProduct, e.detail.value);
    //     })
    //     .catch(err => tagProduct.dropdown.hide.call(tagProduct))
}

function validate_input(input_id){
    var input_val = $('#register-form').find(input_id).val();
    if(!input_val){
        return false;
    }
    return;
}
function final_validates() {
    jQuery('#form-validates').click(function(event) {
        jQuery('.error-flag').addClass('hidden');
        var form = jQuery('#register-form');
        let stop = 0;
        jQuery.each(jQuery("input.validate-input"),function(){
            let element_class = "."+jQuery(this).attr("id");
            let element_id =  "#"+jQuery(this).attr("id");
            var validate_result = validate_input(element_id);
            if(validate_result == false){
                jQuery(".error-flag"+element_class).removeClass('hidden');
                scrollTo(element_id);
                stop = 1;
                console.log('stop');
                return false;
            }
        });
        if(stop == 1){
            return
        }
    if(stop == 1){
        return
    }  
    let payload = "";
    payload = new FormData();
    let special_request = {};
    let display_reqeust = {};
    $.each($("#interest-group .form-check-input:checked"),function(){
        let key = $(this).attr("data-check");
        special_request[key] = 1;
        display_reqeust[key] = $(this).siblings('label').text().replace(/\s/g, "")
    });
    data = {
        company_id              : form.find('#companyid').val(),
        company_name            : form.find('#companyname').val(),
        company_email   		: form.find('#companyemail').val(),
        company_type   	        : form.find('#company_type').val(),
        company_description     : form.find('#company-description').val(),
        product   		        : form.find('#product').val(),
        product_category        : form.find('#product-category').val(),
        product_description     : form.find('#product-description').val(),
        product_quantity        : form.find('input[name="product_quantity"]:checked').val(),
        product_image   	    : form.find('#product_image').val(),
        product_target          : form.find('#product-target').val(),
        contact_name   	        : form.find('#contactname').val(),
        contact_address         : form.find('#contactaddress').val(),
        contact_phone           : form.find('#contactphone').val(),
        company_website         : form.find('#contactwebsite').val(),
        interest_description    : special_request,
    };
    $.each($('.summary-answer'),function(){
        //show summary in modal
        let key = $(this).attr('data-key');
        let output = '';
        //auto populate data
        if(key == 'product' || key == 'product_category'){
            //convert back to object
            let data_obj = JSON.parse(data[key]);

            //get only value, put in output var
            for (const [k, v] of Object.entries(data_obj)) {
                output += v.value + ", ";
            }
            //print output
            $(this).text(output); 
        }else if(key == "interest_description"){
            output = Object.values(display_reqeust);
            $(this).text(output); 
        }
        else{
            $(this).text(data[key]); 
        }
        
    });

    payload.append('action',"register_jline");
    payload.append('security',tagy_obj.security);
    //sending mixed data need to manual json
    packeddata = JSON.stringify(data);
    payload.append('data',packeddata);
    if(jQuery('#product_image').val() != 0){
        var file = jQuery('#product_image')[0].files[0];
        let reader = new FileReader();
        reader.onload = function(e) {
            jQuery('#productimgoutput').attr('src', e.target.result);
          }
        //read file and preview
        reader.readAsDataURL(file);
        //jQuery("#productimgoutput").attr('src',reader.result);
        //add file to payload
        if(file){
            payload.append('file',file);
        }
    }

        //validate data
   
    jQuery('#myModal').modal()
    submit_jline(payload);

    });
}

function submit_jline(payload){
    console.log(payload); 
    jQuery('#form-submits,#form-edits').click(function(event) {
        jQuery('#form-submits,#form-edits').prop('disabled', true);
        jQuery('#myModal').modal('hide');
        let this_action = jQuery(this).attr("id");
        event.preventDefault();
        //payload.append('this_action', this_action);
        jQuery.ajax({
            url: tagy_obj.ajax_url,
            type : 'post',
            //processData: false,
            contentType: false,
            processData: false,
            datatype : 'json',
            data : payload,
            success: function(response) {
                jQuery('#form-submits,#form-edits').prop('disabled', false);
                response = JSON.parse(response);
                console.log(response);
                switch (response.status) {
                    case 'success':
                        //window.location.replace("/thank-you/");
                        break;
                    case 'error':
                        console.log(response.msg);
                        break;
                    default:
                        break;
                }

            }
        });
    });
}
final_validates();