$(document).ready(function() {
    $('#the-table').DataTable({
        //"colReorder"  : true,
        dom: 'Bfrtip',
        //"scrollY"     : 1200,
        "scrollX"   : true,
        //"scrollCollapse": true,
        //"scroller"    : true,
        "pageLength": 50,
        "buttons": [
            'copy', 'csv', 'excel'
        ]
        
    });
} );