
// Image Functions
function showImage(imgPath, imgText, galleryId) { 
  var curImage = document.getElementById('currentImg-' + galleryId);      
  curImage.src = imgPath;
  curImage.alt = imgText;
  curImage.title = imgText;
}

function changeActiveState(value) { 
  var el = document.getElementById('carousel-' + value);
  var list = el.getElementsByTagName("img");
  var curImage = document.getElementById('currentImg-' + value);      
  curImage.src = list[1].src;
  curImage.alt = list[1].alt;
  curImage.title = list[1].alt;
  var arr = document.getElementsByClassName('carousel-item');
  for (var i = 0; i < arr.length; i++){
      arr[i].classList.remove('active');  
  }
  el.classList.add('active');  
}
var langset = [];
var selected_lang = null;
let main = document.getElementById("main-container");
let is_thai = main.classList.contains("form-thai");
let is_jp = main.classList.contains("form-jp");
if(is_thai){
    selected_lang = "th_TH";
}
if(is_jp){
    selected_lang = "ja_JP";
}
get_translate_text(selected_lang);
var elements = document.getElementsByClassName("translate-button");
for (var i = 0; i < elements.length; i++) {
    elements[i].addEventListener('click', select_language, false);
}
function select_language() {
    var selected_lang = this.getAttribute("data-lang");
    let main = document.getElementById("main-container");
    let is_thai = main.classList.contains("form-thai");
    let is_jp = main.classList.contains("form-jp");
    if(selected_lang =="ja_JP" && is_thai){
        return
    }
    if(selected_lang == "th_TH" && is_jp){
        return
    }
    get_translate_text(selected_lang);
};

function get_translate_text(lang){
    let url = 'http://local.matching//wp-content/themes/cmatch/assets/languages/'+lang+'.json';
    fetch(url)
        .then((response) => {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                response.status);
                return;
            }
            response.json().then((data) => {
                langset = data;
                
            })
            .then( () => {
                apply_translation(); 
            })
            }
        )
        .catch((err) => {
            console.log('error' + err);
        });
}
function apply_translation(){
    var alltag = document.getElementsByClassName("lang-tag");
    for (let [key, thisdom] of Object.entries(alltag)) {
        let lang_key = thisdom.getAttribute('data-lang'); 
        if(thisdom.tagName == "INPUT"){ // change place holder
            if(langset["placeholder"][lang_key]){
                thisdom.placeholder = langset["placeholder"][lang_key];;         
            }
        }else{ // change normal text
            if(langset[lang_key]){
                thisdom.innerHTML = langset[lang_key];
            } 
        }

       
    }
}
$(document).ready(function(){
    $('.menu-home-link').click(function(e) {
        e.preventDefault();
        let target  = $(this).find("a").attr("href");
        $("html, body").animate({ // catch the `html, body`
            scrollTop: $(target).offset().top - 100 // button's offset - 10 
        }, 1000); // in 1000 ms (1 second)
    });		
});

