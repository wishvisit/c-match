var config = {
    apiKey: "AIzaSyDgrS1vCPdPQvG9IJD-qIowRokUIyKr9HQ",
    authDomain: "c-match-63979.firebaseapp.com",
    databaseURL: "https://c-match-63979.firebaseio.com",
    projectId: "c-match-63979",
    storageBucket: "c-match-63979.appspot.com",
    messagingSenderId: "13759677809",
    appId: "1:13759677809:web:8933358989d2be3898a2d0",
    measurementId: "G-WKMX89QSHG"
};
firebase.initializeApp(config);
firebase.analytics();


var uiConfig = {
    signInOptions: [
        // Specify providers you want to offer your users.	
        // firebase.auth.FacebookAuthProvider.PROVIDER_ID,
        // firebase.auth.TwitterAuthProvider.PROVIDER_ID,
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.EmailAuthProvider.PROVIDER_ID
    ],
    // Terms of service url can be specified and will show up in the widget.
    tosUrl: 'http://www.c-match.biz/term-and-condition',
    privacyPolicyUrl: 'http://www.c-match.biz/term-and-condition',
    callbacks: {
        'signInSuccessWithAuthResult': function(currentUser, credential, redirectUrl) {
            if(currentUser.additionalUserInfo.profile){
                //google mail
                var userData = {
                    'isNewUser': currentUser.additionalUserInfo.isNewUser,
                    'email': currentUser.additionalUserInfo.profile.email,
                    'name': currentUser.additionalUserInfo.profile.name,
                    'id': currentUser.additionalUserInfo.profile.id,
                    'photo': currentUser.additionalUserInfo.profile.picture,
                    'uid': currentUser.user.uid,
                };
            }else{
                //normal mail
                var userData = {
                    'isNewUser': currentUser.additionalUserInfo.isNewUser,
                    'email': currentUser.user.email,
                    'name': currentUser.user.displayName,
                    'id': currentUser.user.uid,
                    //'photo': currentUser.additionalUserInfo.profile.picture,
                    'uid': currentUser.user.uid,
                };
            }
            
            getUser(userData);
        },
    }
};


$(document).ready(function() {
    final_validate();
    var company_data;
    var ui = new firebaseui.auth.AuthUI(firebase.auth());
    if ($('#firebaseui-auth-container').length) {
        ui.start('#firebaseui-auth-container', uiConfig);
    }
});

function getUser(userData) {
    //console.log('user_data: ', userData);
    var event_id = $('#event-id').val();
    $.ajax({
        url: login_ajax_obj.ajax_url,
        type: 'post',
        data: {
            action: 'firebase_ajax_login',
            userdata: userData,
            event_id : event_id
        },
        success: function(response) {
            response = JSON.parse(response);
            switch (response.status) {
                case 'success':
                    window.location.reload();
                    console.log('success');
                    break;
                case 'error':
                    console.log(response.msg);
                    break;
                default:
                    break;
            }

        }
    });
}

function update_province() {
    var province = $('#provinceval').text();
    if (province.length > 0) {
        $('.province-option[value=' + province + ']').prop("selected", "selected");
    }
}
function scrollTo(element_id){
    $([document.documentElement, document.body]).animate({
        scrollTop: $(element_id).offset().top-200
    }, 2000);
}
function validate_input(input_id){
    var input_val = $('#register-form').find(input_id).val();
    if(!input_val){
        return false;
    }
    return;
}
function validate_check_box_group(group_id){
    let check_class = group_id+" input[type=checkbox]:checked"
    let checkgroup = $(check_class).length;
    if(checkgroup == 0){
        return false;
    }
    return true;
}
function validate_other(group_id){
    let class_other = group_id+" input.other-validate";
    let class_checkbox = group_id+" input.input-other[type=checkbox]:checked"
    let is_other_checked = $(class_checkbox).length;
    let other_val = true;
    let is_other_exist = $(class_other).length;
    if(is_other_exist){
        other_val = $(class_other).val();
    }
    
    if(is_other_checked && !other_val){
       return false; 
       //check other but not fill data
    }
    return true;
}

function final_validate() {
    $('#form-validate').click(function(event) {
        $('.error-flag').addClass('hidden');
        var form = $('#register-form');
        let stop = 0;
        $.each($(".validate-input"),function(){
            let element_class = "."+$(this).attr("id");
            let element_id =  "#"+$(this).attr("id");
            var validate_result = validate_input(element_id);
            if(validate_result == false){
                $(".error-flag"+element_class).removeClass('hidden');
                scrollTo(element_id);
                stop = 1;
                console.log('stop');
                return false;
            }
        });
        if(stop == 1){
            return
        }
    $.each($(".check-box-group-validate"),function(){
        let element_tag  = $(this).attr("id");
        let element_class = "."+$(this).attr("id");
        let element_id =  "#"+$(this).attr("id");
        if(validate_check_box_group(element_id) && validate_other(element_id)){
            $.each($(element_id + " input[type=checkbox]:checked"), function(){
                element_tag[$(this).attr("data-check")] = 1;
            });
        }else{
            stop = 1 ;
            $(".error-flag"+element_class).removeClass('hidden');
            scrollTo(element_id);
            return false;
        }
    })
    if(stop == 1){
        return
    }
    let company_country = form.find('#companycountry').val();
    let company_type = {};
    $.each($("#company-type-group .form-check-input:checked"),function(){
        let key = $(this).attr("data-check");
        company_type[key] = 1;
    });
    let company_type_data = JSON.stringify(company_type);
    
    let company_customer_group = {};
    $.each($("#company-customer-group .form-check-input:checked"),function(){
        let key = $(this).attr("data-check");
        company_customer_group[key] = 1;
    });
    let company_customer_group_data = JSON.stringify(company_customer_group);    
        
    let your_budget = {};
    $.each($("#your-budget .form-check-input:checked"),function(){
        let key = $(this).attr("data-check");
        your_budget[key] = 1;
    });
    //let your_budget_data = JSON.stringify(your_budget);    

    let interest_company_group = {};
    $.each($("#interest-company-group .form-check-input:checked"),function(){
        let key = $(this).attr("data-check");
        interest_company_group[key] = 1;
    });
    //let interest_company_group_data = JSON.stringify(interest_company_group);    
    
    let interest_activity_group = {};
    $.each($("#interest-activity-group .form-check-input:checked"),function(){
        let key = $(this).attr("data-check");
        interest_activity_group[key] = 1;
    });
    //let interest_activity_group_data = JSON.stringify(interest_activity_group);

    let expect_budget = {};
    $.each($("#expect-budget .form-check-input:checked"),function(){
        let key = $(this).attr("data-check");
        expect_budget[key] = 1;
    });
    //let expect_budget_data = JSON.stringify(expect_budget);  

    company_data = {
        company_id              : form.find('#companyid').val(),
        company_country         : company_country,//json
        company_name            : form.find('#companyname').val(),
        company_phone           : form.find('#companyphone').val(),
        company_email           : form.find('#companyemail').val(),
        company_website         : form.find('#companywebsite').val(),
        company_type            : company_type, //json
        company_type_other      : form.find('#company-type-other').val(),
        company_customer_type   : company_customer_group, //json
        company_customer_other  : form.find('#company-customer-other').val(),
        your_budget             : your_budget,//json
        company_description     : form.find('#company-description').val(),
        contact_name            : form.find('#contactname').val(),
        contact_position        : form.find('#contactposition').val(),
        contact_phone           : form.find('#contactphone').val(),
        contact_email           : form.find('#contactemail').val(),
        interest_company        : interest_company_group, //json
        interest_company_other  : form.find('#interest-company-other').val(),
        interest_activity       : interest_activity_group, //json
        interest_activity_other : form.find('#interest-activity-other').val(),
        expect_budget           : expect_budget,//json
        interest_description    : form.find('#interestdescription').val(),
    };
    //validate data
    $.each($('.summary-answer'),function(){
        //show summary in modal
        let key = $(this).attr('data-key');
        let output = '';
        //auto populate data
        if(typeof company_data[key] === 'object' && company_data[key] !== null){
            for (const [k, v] of Object.entries(company_data[key])) {
                output += langset[k];
                output += " "; 
              }
            $(this).text(output); 
        }else{
            $(this).text(company_data[key]); 
        }
       
    });
    $('#myModal').modal()
    submit_data_active();
    });
}

function submit_data_active(){
    console.log(company_data); 
    $('#form-submit,#form-edit').click(function(event) {
        $('#form-submit,#form-edit').prop('disabled', true);
        $('#myModal').modal('hide');
        let this_action = $(this).attr("id");
        event.preventDefault();
        company_data.this_action = this_action;
    $.ajax({
        url: login_ajax_obj.ajax_url,
        type: 'post',
        data: {
            action: 'register_new_company',
            company_data: company_data
        },
        success: function(response) {
            $('#form-submit,#form-edit').prop('disabled', false);
            response = JSON.parse(response);
            console.log(response);
            switch (response.status) {
                case 'success':
                    window.location.replace("/thank-you/");
                    break;
                case 'error':
                    console.log(response.msg);
                    break;
                default:
                    break;
            }

        }
    });
    });
}