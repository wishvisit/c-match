<?php
/**
* Template Name: Register
 *
 */
get_template_part( 'template-parts/cmatch-header' );?>
<main id="main-container" class="home-page-container">
    <?php get_template_part( 'template-parts/cmatch-menu' );?>
    <section id="register-title" class="full-page-background">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="home-logo">
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/dummy-register.png"
                            alt="dummy image" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="register-sponcer">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="sponcer-title section-title font-prompt">Sponcered By</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 text-right" id="">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/register-sponcer1.png"
                        alt="sponcer logo" />
                </div>
                <div class="col-12 col-md-6" id="">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/register-sponcer2.png"
                        alt="sponcer logo" />
                </div>
            </div>
        </div>
    </section>
    <section id="matching-details">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <img class="shake-hand"
                        src="<?php echo get_template_directory_uri();?>/assets/images/hand-shake.png"
                        alt="hand shake" />
                    <h2 class="matching-details-title font-prompt">Online Business Matching</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 event-date text-center small-logo" id="">
                    <img class="calendar" src="<?php echo get_template_directory_uri();?>/assets/images/carlendar.png"
                        alt="calendar logo" />
                    <p class="matching-date font-prompt">18 December 2020</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 event-time text-center small-logo" id="">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/clock.png" alt="sponcer logo" />
                    <p class="matching-time font-prompt">19:00 - 24:00</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 event-location text-center small-logo" id="">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/location-pin.png"
                        alt="sponcer logo" />
                    <p class="matching-location font-prompt">Online Matching</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 event-description text-center" id="">
                    <p class="matching-description font-prompt text-left">นครนาโงยะ (ญี่ปุ่น: 名古屋市 โรมาจิ: นาโงยะชิ)
                        เป็นเมืองที่ใหญ่ที่สุดในภาคชูบุ และมีประชากรมากเป็นอันดับ 4 ของประเทศญี่ปุ่น
                        ตั้งอยู่บนชายฝั่งมหาสมุทรแปซิฟิกบนเกาะฮนชู
                        เป็นเมืองเอกของจังหวัดไอจิและเป็นหนึ่งในเมืองท่าหลักของญี่ปุ่น ซึ่งประกอบด้วย โตเกียว โอซากะ
                        โคเบะ โยโกฮามะ ชิบะ และคิตะกีวชู
                        นอกจากนี้ยังเป็นศูนย์กลางของเขตมหานครที่ใหญ่เป็นอันดับสามของญี่ปุ่นที่เรียกว่า เขตมหานครรชูเกียว
                        ซึ่งมีประชากรกว่า 9.1 ล้านคน</p>
                </div>
            </div>
        </div>
    </section>
    <section id="register-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="sponcer-title section-title font-prompt lang-tag" data-lang="register_here">
                        ลงทะเบียนที่นี่</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 text-center border-right" id="">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/thai-flag.png" alt="Thai flag" />
                    <p class="text-center register-label"> THAI <br> COMPANY</p>
                    <button type="button" class="register-button" data-toggle="modal" data-target="#register">
                        ลงทะเบียน
                    </button>
                </div>
                <div class="col-12 col-md-6 text-center" id="">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/japan-flag.png"
                        alt="Japanese Flag" />
                    <p class="text-center register-label"> JAPANESE <br> COMPANY</p>
                    <button type="button" class="register-button" data-toggle="modal" data-target="#register">
                        登録
                    </button>
                </div>
            </div>
        </div>
        <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="register"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="register">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="how-to">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="how-to-title1 section-title font-prompt lang-tag" data-lang="how_to_title1">ขั้นตอนง่ายๆ 
                        <span class="how-to-title2 lang-tag" data-lang="how_to_title1">ในการจับคู่กาาเจรจาธุรกิจ</span>
                    </h2>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-3 text-center" id="how-to-step1">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/how-to-regist.png"
                        alt="register logo" />
                    <h4 class="how-to-subtitle font-prompt lang-tag" data-lang="how_to_register_title">Registration</h4>
                    <p class="ho-to-date-time font-prompt">10 - 14 มค 2563</p>
                    <p class="how-to-description font-prompt lang-tag" data-lang="how_to_register_description">ช่วงเปิดรับลงทะเบียน ลงทะเบียนกรอกรายละเอียดข้อมูลของบริษัทของท่าน</p>
                </div>
                <div class="col-12 col-md-1 text-center how-to-arrow v-top d-none d-md-block">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/how-to-next.png"
                        alt="next logo" />
                </div>
                <div class="col-12 col-md-3 text-center" id="how-to-step1">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/how-to-select.png"
                        alt="select logo" />
                    <h4 class="how-to-subtitle font-prompt lang-tag" data-lang="how_to_select_title">SELECTION & MATCHING</h4>
                    <p class="ho-to-date-time font-prompt">15 - 17 มค 2563</p>
                    <p class="how-to-description font-prompt lang-tag" data-lang="how_to_select_description">ช่วงเลือกและจับคู่ทางธุรกิจ รอรับผลการจับคู่ผ่านทางอีเมล</p>
                </div>
                <div class="col-12 col-md-1 text-center how-to-arrow v-top d-none d-md-block">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/how-to-next.png"
                        alt="next logo" />
                </div>
                <div class="col-12 col-md-3 text-center" id="how-to-step1">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/how-to-event.png"
                        alt="event logo" />
                    <h4 class="how-to-subtitle font-prompt lang-tag" data-lang="how_to_event_title">EVENT DATE</h4>
                    <p class="ho-to-date-time font-prompt">18 มค 2563</p>
                    <p class="how-to-description font-prompt lang-tag" data-lang="how_to_event_description">เข้ามาเจรจาธุรกิจผ่านทางออนไลน์ ตามวันและเวลาที่กำหนดไว้</p>
                </div>
            </div>
        </div>
    </section>
</main>
<?php get_template_part( 'template-parts/cmatch-footer' );?>