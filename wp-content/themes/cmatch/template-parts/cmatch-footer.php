
    <footer class="c-matching-footer">
        <div id="footer-logo">
            <a href="<?php echo get_home_url();?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/cmatch-logo.png" alt="cmatch logo"/></a>
        </div>
    </footer>

    <?php wp_footer(); ?>
</body>
</html>