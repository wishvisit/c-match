<?php
/**
* Template Name: Cmatch Home
 *
 */

get_template_part( 'template-parts/cmatch-header' );?>


<main id="main-container" class="home-page-container prompt">
    <?php get_template_part( 'template-parts/cmatch-menu' );?>
    <section id="home-title" class="full-page-background">
        <video id="home-bg" autoplay muted loop>
            <source src="<?php echo get_template_directory_uri();?>/assets/videos/home-bg.mp4" type="video/mp4">
        </video>
         <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="home-logo">
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/cmatch-logo.png" alt="cmatch logo"/>
                    </div>
                    <div class="home-sublogo text-center">
                        <div class="home-sublogo-img"><img src="<?php echo get_template_directory_uri();?>/assets/images/sub-logo-img.png" alt="cmatch logo"/></div>
                        <p class="prompt-light pt-3">Online Business Matching</p>
                        <p class="prompt-light">Between Japan and Thailand</p>
                        <p class="font-kozuka pt-2">タイと日本をつなぐ</p>
                        <p class="font-kozuka">オンラインビジネスマッチング</p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <section id="home-about" class="home-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6" id="home-about-img">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/cmatch-home-aboutus.png" alt="cmatch logo"/>
                </div>
                <div class="col-12 col-md-6" id="home-about-text">
                    <h4 class="home-about-header text-center">ONLINE <br> BUSINESS MATCHING</h4>
                    <p class="lang-tag" data-lang="home_about_description">Online Business Matching ของ CMatch เป็นแพลทฟอร์มทางธุรกิจที่ช่วยสนับสนุนและสร้างโอกาสให้คุณได้พบเจอพร้อมกับเจรจาธุรกิจผ่านช่องทางออนไลน์ ไม่ว่าคุณจะอยู่ที่ไหนในมุมโลก คุณสามารถสร้างคู่ค้าที่ช่วยผลักดันสู่จุดสูงสุดและเติบโตอย่างไม่สิ้นสุดไปพร้อมๆกัน</p> 
                </div>
            </div>
        </div>
    </section>
    <section id="home-benefit" class="home-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="benefit-title section-title prompt-semi lang-tag" data-lang="benefit_title">BENEFIT</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-6 col-md-3" id="home-benefit1">
                    <img class="benefit-img" src="<?php echo get_template_directory_uri();?>/assets/images/benefit1.png" alt="business matching benefit">
                    <h3 class="benefit-subtitle text-center prompt-semi lang-tag" data-lang="benefit_head1">Create Opportunity</h3>
                    <p class="benefit-content text-center prompt-extralight lang-tag" data-lang="benefit1">
                    สร้างโอกาสในการทำธุรกิจให้หลากหลายและกว้างไกล
                    </p>
                </div>
                <div class="col-6 col-md-3" id="home-benefit2">
                    <img class="benefit-img" src="<?php echo get_template_directory_uri();?>/assets/images/benefit2.png" alt="business matching benefit">
                    <h3 class="benefit-subtitle text-center prompt-semi lang-tag" data-lang="benefit_head2">Contact</h3>
                    <p class="benefit-content text-center prompt-extralight lang-tag" data-lang="benefit2">
                    ข้อมูลในการติดต่อทุกธุรกิจที่ครบถ้วน และ ไม่จำกัดแค่ธุรกิจในประเทศไทย
                    </p>
                </div>
                <div class="col-6 col-md-3" id="home-benefit3">
                    <img class="benefit-img" src="<?php echo get_template_directory_uri();?>/assets/images/benefit3.png" alt="business matching benefit">
                    <h3 class="benefit-subtitle text-center prompt-semi lang-tag" data-lang="benefit_head3">Convenience</h3>
                    <p class="benefit-content text-center prompt-extralight lang-tag" data-lang="benefit3">
                    เข้าถึงทุกธุรกิจได้ง่ายและสะดวกในการเชื่อมต่อบนโลกออนไลน์
                    </p>
                </div>
                <div class="col-6 col-md-3" id="home-benefit4">
                    <img class="benefit-img" src="<?php echo get_template_directory_uri();?>/assets/images/benefit4.png" alt="business matching benefit">
                    <h3 class="benefit-subtitle text-center prompt-semi lang-tag" data-lang="benefit_head4">Coordination</h3>
                    <p class="benefit-content text-center prompt-extralight lang-tag" data-lang="benefit4">
                    การประสานงานของทีมงานทุกคนที่มีเป้าหมายให้คุณได้รับความพึงพอใจสูงสุด
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="home-whyus">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="section-title whyus-title lang-tag" data-lang="whyus_title">WHY US?</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-6 col-md-3" id="home-whyus1">
                    <img class="whyus-img pb-3" src="<?php echo get_template_directory_uri();?>/assets/images/whyus1.png" alt="why cmatch business matching">
                    <p class="whyus-content text-center prompt-extralight lang-tag" data-lang="whyus1">
                    เป็นผู้จัดงาน Business Matching ให้ธุรกิจจากญี่ปุ่น
                    </p>
                </div>
                <div class="col-6 col-md-3" id="home-whyus2">
                    <img class="whyus-img pb-3" src="<?php echo get_template_directory_uri();?>/assets/images/whyus2.png" alt="why cmatch business matching">
                    <p class="whyus-content text-center prompt-extralight lang-tag" data-lang="whyus2">
                    มีบริษัทชั้นนำที่สนใจหาคู่ค้าทางธุรกิจทั้งในไทยและญี่ปุ่นมากมาย 
                    </p>
                </div>
                <div class="col-6 col-md-3" id="home-whyus3">
                    <img class="whyus-img pb-3" src="<?php echo get_template_directory_uri();?>/assets/images/whyus3.png" alt="why cmatch business matching">
                    <p class="whyus-content text-center prompt-extralight lang-tag" data-lang="whyus3">
                    มีประสบการณ์ในการสนับสนุนธุรกิจกว่า 10 ปี 
                    </p>
                </div>
                <div class="col-6 col-md-3" id="home-whyus4">
                    <img class="whyus-img pb-3" src="<?php echo get_template_directory_uri();?>/assets/images/whyus4.png" alt="why cmatch business matching">
                    <p class="whyus-content text-center prompt-extralight lang-tag" data-lang="whyus4">
                    ทีมงานคอยดูแลทุกขั้นตอน และช่วยเหลือด้านแปลภาษาญี่ปุ่นเชิงธุรกิจ 
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="home-before-feed" class="full-page-background">
         <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="home-before-feed-img">
                        <img class="before-feed" src="<?php echo get_template_directory_uri();?>/assets/images/whyusimg.png" alt="cmatch business matching">
                    </div>
                </div>         
            </div>
        </div>
    </section>
    <section id="home-coming-event">
        <div class="container pb-5">
            <div class="row">
                <div class="col-12">
                    <img class="upcoming-img" src="<?php echo get_template_directory_uri();?>/assets/images/upcoming.png" alt="cmatch business matching" /><h2 class="feed-title upcoming-title prompt-semi green1 lang-tag" data-lang="upcoming">UPCOMING EVENT</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 coming-event-feed">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/upcoming.jpg" alt="cmatch business matching" />
                </div> 
            </div>
            <section id="home-register" class="bg-white">
				<div class="col-12 col-lg-8 center pt-5 pb-5">
				  <h3 class="col-12 txt-center pb-5 lang-tag" data-lang="register_here">ลงทะเบียนที่นี่</h3>
					<div class="row">
						<div class="col-6 text-center">
							<img src="<?php echo get_template_directory_uri();?>/assets/images/C Match-66.png" class="center flag pb-3">
							<p class="txt-center lang-tag" data-lang="thai_company">THAI<br>COMPANY</p>
              <a href="/thai-company" class="register-button">
                <span class="txt-center lang-tag registerbutton" data-lang="register_button">ลงทะเบียน</span>
              </a>
						</div>
						<div class="col-6 text-center">
							<img src="<?php echo get_template_directory_uri();?>/assets/images/C Match-67.png" class="center flag pb-3">
							<p class="txt-center lang-tag" data-lang="japan_company">JAPAN<br>COMPANY</p>
              <a href="/japan-company" class="register-button">
                <span class="txt-center lang-tag registerbutton" data-lang="register_button">ลงทะเบียน</span>
              </a>
						</div>
					</div>
			   </div>
			</section>
        </div>
         
    </section>
   
    <section id="home-last-event">
        <div class="container">
            <div class="row">
                <div class="col-12 ">
                    <img class="pastevent-img" src="<?php echo get_template_directory_uri();?>/assets/images/pastevent.png" alt="cmatch business matching"><h2 class="feed-title pastevent-title prompt-semi green1 lang-tag" data-lang="past_event">PAST EVENT</h2>
                </div>
            </div>
			 <div class="row m-0">

			   <section>             
				<div class="col-12">
				  <div class="row">           

				  <!-- Gallery Item Loop  -->

					<div onclick="changeActiveState(1)" class="col-md-6 col-lg-4 py-4" data-toggle="modal" data-target="#exampleModal">
					  <div class="gallery-container">
					  <img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/1.jpg" alt="Alt Name" class="image">
						<p class="pt-3 txt-center">Okinawa Seminar & Business Matching 2020</p>
						 <div class="overlay">
						  <p class="text p-2" style="width: 80%;">
							<span>Okinawa Seminar & Business Matching 2020</span><br>

							<button class="btn view-btn">VIEW</button>
						  </p>                            
						 </div>

					  </div>
					</div> 

				  <!--  End  -->

					<div onclick="changeActiveState(2)" class="col-md-6 col-lg-4 py-4" data-toggle="modal" data-target="#exampleModal">
						<div class="gallery-container">
						   <img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/1.jpg" alt="Alt Name" class="image">
							<p class="pt-3 txt-center">Centrair Seminar & Business Matching 2018</p>
						   <div class="overlay">
							  <p class="text p-2" style="width: 80%;">
								 <span>Centrair Seminar & Business Matching 2018</span><br>
								 <button class="btn view-btn">VIEW</button>
							  </p>                            
						   </div>

						</div>
					</div> 

					<div onclick="changeActiveState(3)" class="col-md-6 col-lg-4 py-4" data-toggle="modal" data-target="#exampleModal">
						<div class="gallery-container">
						   <img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/1.jpg" alt="Alt Name" class="image">
							<p class="pt-3 txt-center">Okinawa Seminar & Business Matching 2015</p>
						   <div class="overlay">
							  <p  class="text p-2" style="width: 80%;">
								 <span>Okinawa Seminar & Business Matching 2015</span><br>
								 <button class="btn view-btn">VIEW</button>
							  </p>                            
						   </div>

						</div>
					 </div>

					</div>
				   </div>
			  	 </section>



						<!-- MODAL -->
						<div class="modal fadeIn" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						 <div class="modal-dialog modal-lg modal-dialog-centered" role="document">   

						  <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
						   <div class="carousel-inner">

							 <!--  Item 1 Loop  -->
							 <div class="carousel-item active" id="carousel-1">
							  <div class="modal-content">
								 <div class="modal-header">
									<h5 class="modal-title txt-center w-100" id="exampleModalLabel">Okinawa Seminar & Business Matching 2020</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									   <span aria-hidden="true">&times;</span>
									</button>
								 </div>

								 <div class="modal-body">
									<img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/1.jpg" id="currentImg-1" Title="" alt="Alt Name" class="image">
								 </div>

								 <div class="modal-thumbs px-3 py-3">
									<label>Gallery Images</label>
									<ul>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/1.jpg','Title',1);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/1.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/2.jpg','Title',1);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/2.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/3.jpg','Title',1);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/3.jpg" alt="Alt Name" class="image"></div>                  
									   </li>

									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/4.jpg','Title',1);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/4.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/5.jpg','Title',1);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/5.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/6.jpg','Title',1);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/6.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/7.jpg','Title',1);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/7.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/8.jpg','Title',1);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/8.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/9.jpg','Title',1);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/9.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/10.jpg','Title',1);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2020/10.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									</ul>
								 </div>              
							  </div>
							 </div>  
							 <!-- Item 1 End  -->

							 <!--  Item 2 Loop  -->
							 <div class="carousel-item" id="carousel-2">
							  <div class="modal-content">
								 <div class="modal-header">
									<h5 class="modal-title txt-center w-100" id="exampleModalLabel">Centrair Seminar & Business Matching 2018</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									   <span aria-hidden="true">&times;</span>
									</button>
								 </div>

								 <div class="modal-body">
									<img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/1.jpg" id="currentImg-2" alt="Alt Name" class="image">
								 </div>

								 <div class="modal-thumbs px-3 py-3">
									<label>Gallery Images</label>
									<ul>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/1.jpg','Title',2);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/1.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									    <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/2.jpg','Title',2);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/2.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/3.jpg','Title',2);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/3.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/4.jpg','Title',2);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/4.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/5.jpg','Title',2);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/5.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/6.jpg','Title',2);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/6.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/7.jpg','Title',2);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/7.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/8.jpg','Title',2);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/8.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/9.jpg','Title',2);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/9.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/10.jpg','Title',2);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/centrair/10.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									</ul>
								 </div>              
							  </div>
							 </div>  
							 <!-- Item 2 End  -->

							 <!--  Item 3 Loop  -->
							 <div class="carousel-item" id="carousel-3">
							  <div class="modal-content">
								 <div class="modal-header">
									<h5 class="modal-title txt-center w-100" id="exampleModalLabel">Okinawa Seminar & Business Matching 2015</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									   <span aria-hidden="true">&times;</span>
									</button>
								 </div>

								 <div class="modal-body">
									<img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/1.jpg" id="currentImg-3" alt="Alt Name" class="image">
								 </div>

								 <div class="modal-thumbs px-3 py-3">
									<label>Gallery Images</label>
									<ul>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/1.jpg','Title',3);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/1.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/2.jpg','Title',3);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/2.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/3.jpg','Title',3);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/3.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/4.jpg','Title',3);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/4.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/5.jpg','Title',3);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/5.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/6.jpg','Title',3);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/6.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/7.jpg','Title',3);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/7.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/8.jpg','Title',3);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/8.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/9.jpg','Title',3);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/9.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									   <li class="px-1" onclick="showImage('<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/10.jpg','Title',3);">
										  <div class="border"><img src="<?php echo get_template_directory_uri();?>/assets/images/past-event/okinawa2015/10.jpg" alt="Alt Name" class="image"></div>                  
									   </li>
									</ul>
								 </div>              
							  </div>
							 </div>  
							 <!-- Item 3 End  -->


						   </div> 
						  </div><!-- Carousel End -->

						</div>   

						<!-- Prev Next Arrows -->
						<a href="#carouselExampleFade" role="button" data-slide="prev" class="carousel-control-prev"><div class="arrow-left d-flex align-items-center text-center" href="#carouselExampleFade" role="button" data-slide="prev"><img src="https://www.ravenshoegroup.com/concept/codepen/arrow-left.png" width="20px" alt="Left Arrow"></div></a>
						<a href="#carouselExampleFade" role="button" data-slide="prev" class="carousel-control-next"><div class="arrow-right d-flex align-items-center text-center" href="#carouselExampleFade" role="button" data-slide="next"><img src="https://www.ravenshoegroup.com/concept/codepen/arrow-right.png" width="20px" alt="Right Arrow"></div></a>
						<!--  Arrows End   -->

						</div><!-- Modal End  -->

					</div>
    <section id="home-newreq">
        <div class="container">
            <div class="row">
                <div class="col-5 col-md-6 newreq-img text-center">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/new-req.png" alt="cmatch business matching new request" />
                </div> 
                <div class="col-7 col-md-6 v-center">
                    <h4 class="newreq-title prompt-semi green1 lang-tag" data-lang="newreq_title">CREATE NEW REQUIREMENT</h4>
                    <p class="newreq-text prompt-extralight gray1 lang-tag" data-lang="newreq_text">หากท่านมีความสนใจธุรกิจอื่นๆ<br> สามารถกรอกข้อมูลได้ที่นี่</p>
                    <a class="col-12 col-lg-5 btn border p-2 txt-center btn-light prompt-extralight gray1" href="mailto:info@c-match.biz">CLICK</a>
                </div> 
            </div>
        </div>
    </section>
    <section id="home-footer-text">
        <div class="container">
            <div class="row">
                <div class="col-7 col-md-3 center">
                    <img class="logo-cpoint-footer" src="<?php echo get_template_directory_uri();?>/assets/images/logo-cpoint-footer.png" alt="logo cpoint footer">
                </div> 
                <div class="col-12 col-md-7 v-center">
                    <p class="footer-text lang-tag prompt-extralight pt-3" data-lang="home_footer_text">เรามีพันธกิจและความมุ่งมั่นในการเป็นตัวกลางเชื่อมต่อ ส่งเสริม สนับสนุนการทำธุรกิจ รวมทั้งการขยายตลาดระหว่างธุรกิจไทยและธุรกิจญี่ปุ่น ด้วยประสบการณ์กว่า 10 ปี และทีมงานที่พร้อมช่วยเหลือให้คุณสามารถดำเนินธุรกิจได้อย่างราบรื่นและมีประสิทธิภาพ ไม่ว่าคุณจะอยู่ในประเทศไทยหรือญี่ปุ่น</p>
                    <ul class="col-12 p-0 d-flex">
						          <li><a href="https://www.facebook.com/cpointth/" target="_blank"><img src="<?php echo get_template_directory_uri();?>/assets/images/C Match-33.png"></a></li>
                    	<li><a href="https://www.c-point.co.th/th/" target="_blank"><img src="<?php echo get_template_directory_uri();?>/assets/images/C Match-35.png"></a></li>
                    </ul>
                </div> 
            </div>
        </div>
    </section>
</main>



<style>
.txt-center{text-align: center;}
.center{margin: 0 auto;display: table}
.bg-light {
  background-color: #f1f1f1;
}

.gallery-container {
  position: relative;
  width: 100%;
  cursor: pointer;
}

.image {
  display: block;
  width: 100%;
  height: auto;
}

.overlay {
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: #ffffff;
  overflow: hidden;
  width: 100%;
  height: 0;
  transition: 0.3s ease;
}

.gallery-container:hover .overlay {
  height: 50px;
}

.text {
  font-family: "Noto Sans KR", sans-serif;
  font-size: 14px;
  line-height: 1.3em;
  color: #414141;
  font-weight: bold;
}
.text .small {
  font-family: "Noto Sans KR", sans-serif;
  font-size: 13px;
  line-height: 1.3em;
  color: #999999;
}
.text .view-btn {
  right: 10px;
  top: 12px;
  position: absolute;
  color: #000000;
  font-weight: 500;
  border-width: 1px;
  border-color: #e7e7e7;
  border-style: solid;
  font-family: "Noto Sans KR", sans-serif;
  font-size: 13px;
}
.text .view-btn:hover {
  border-color: #333333;
}

.carousel-control-next,
.carousel-control-prev {
  width: auto;
}

.modal-open .modal {
  overflow-x: hidden;
  overflow-y: auto;
  background-color: rgba(0, 0, 0, 0.5);
}

.modal-body {
  position: relative;
  -ms-flex: 1 1 auto;
  flex: 1 1 auto;
  padding: 0rem;
}

.modal-thumbs {
  min-height: 150px;
}
.modal-thumbs label {
  font-family: "Noto Sans KR", sans-serif;
  font-size: 14px;
  line-height: 1.3em;
  font-weight: 700;
  color: #333333;
  padding-left: 5px;
}
.modal-thumbs ul {
  padding: 0;
}
.modal-thumbs ul li {
  list-style: none;
  width: 25%;
  float: left;
}
@media (max-width: 768px) {
  .modal-thumbs ul li {
    width: 50%;
  }
}
.modal-thumbs ul li .border {
  display: inline-block;
  position: relative;
}
.modal-thumbs ul li .border::after {
  content: "";
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  box-shadow: inset 0 0 0 0 rgba(255, 255, 255, 0.5);
  transition: box-shadow 0.1s ease;
  cursor: pointer;
}
.modal-thumbs ul li .border:hover::after {
  box-shadow: inset 0 0 0 10px rgba(255, 255, 255, 0.5);
}
.modal-thumbs ul li img {
  display: block;
  position: relative;
}

.arrow-left {
  float: left;
  left: 0px;
  padding: 50px;
  position: fixed;
  height: 100%;
}
@media (max-width: 768px) {
  .arrow-left {
    display: none !important;
  }
}
.arrow-left:hover {
  background-color: rgba(0, 0, 0, 0.3);
}

.carousel-inner {
  float: left;
}

.arrow-right {
  float: left;
  right: 0px;
  padding: 50px;
  position: fixed;
  height: 100%;
}
@media (max-width: 768px) {
  .arrow-right {
    display: none !important;
  }
}
.arrow-right:hover {
  background-color: rgba(0, 0, 0, 0.3);
}

</style>


<?php get_template_part( 'template-parts/cmatch-footer' );?>