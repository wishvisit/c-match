<?php
/**
* Template Name: Thank you
 *
 */
get_template_part( 'template-parts/cmatch-header' );?>
<main id="main-container" class="home-page-container prompt-regular">
    <?php get_template_part( 'template-parts/cmatch-menu' );?>
   <!-- <section id="register-page-title" class="full-page-background">
         <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="event-title">
                      
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/dummy-thankyou.png" alt="dummy bg" />
                        <?php /* change by script
                        <p class="font-prompt">Welcome to</p>
                        <h2 class="font-prompt title">SAKURA NO SATO</p>
                        <p class="font-prompt sub-title">Event</p> */?>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
      <section id="register-bar">
        <div class="container">
           <div class="col-12 col-lg-8 center">
				<div class="row justify-content-md-center">
					<div class="col col-lg-3">
						<img class="thaiflag" src="<?php echo get_template_directory_uri();?>/assets/images/thai-flag.png" alt="Thai Flag" />
					</div>
					<div class="col col-lg-3">
						<img id="step-register" class="step-logo active" src="<?php echo get_template_directory_uri();?>/assets/images/register/C Match-41-100white.png" alt="Thai Flag" />
					</div>
					<div class="col col-lg-3">
						<img id="step-select" class="step-logo" src="<?php echo get_template_directory_uri();?>/assets/images/register/C Match-45.png" alt="Thai Flag" />
					</div>
					<div class="col col-lg-3">
						<img id="step-date" class="step-logo" src="<?php echo get_template_directory_uri();?>/assets/images/register/C Match-46.png" alt="Thai Flag" />
					</div>
				</div>
            </div>
        </div>
    </section>
    <section class="pt-5">
        <div class="container thankyou">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/thankyou/C Match-76.png" class="w-auto center">
            <div class="txt-center">
				<h1>THANK YOU</h1>
				<h3>REGISTRATION COMPLETED</h3>

			</div>
           	<br>
           	<div class="txt-center">
				<p class="lang-tag" data-lang="thank_line_1">เราจะดำเนินการจับคู่ธุรกิจที่เหมาะสมกับท่านให้โดยเร็วที่สุด</p>
				<p class="lang-tag" data-lang="thank_line_2">และจะติดต่อกลับไปผ่านทางอีเมลที่ท่านได้ให้ไว้</p>
				<p class="lang-tag" data-lang="thank_line_3">ขอขอบคุณสำหรับการลงทะเบียน</p>
           	</div>
           	<div class="txt-center pt-4 pb-4">
           		<p class="lang-tag" data-lang="thank_team">ทีมงาน CMatch</p>
           		<p class="lang-tag" data-lang="thank_ask_info">ท่านสามารถสอบถามเพิ่มเติมได้ที่</p>
           		<p class="lang-tag" data-lang="thank_phone">โทร  02-665-2533</p>
           		<p><a href="mailto:info@c-match.biz" class="lang-tag" data-lang="thank_email">อีเมล  info@c-point.co.th</a></p>
           	</div>
        </div>
    </section>
  
</main>
<?php get_template_part( 'template-parts/cmatch-footer' );?>