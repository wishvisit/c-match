<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>
<main id="main-container" class="site-content container" role="main">
	<?php while ( have_posts() ) :
			the_post();
			?>
			<div class="container default-template-container">
				<div class="row">
					<div class="col-12">
						<h2 class="default-page-title"><?php the_title();?></h2>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<?php the_content();?>
					</div>
				</div>
			</div>
			<?php endwhile; // End the loop.?>
   
</main><!-- #site-content -->
<?php
get_footer();
