<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.c-point.co.th/
 * @since             1.0.1
 * @package           CMatching
 *
 * @wordpress-plugin
 * Plugin Name:       C-point Business Matching
 * Plugin URI:        http://www.c-point.co.th/
 * Description:       C-point Business Matching function and logic core.
 * Version:           1.0.1
 * Author:            Cpoint Thailand
 * Author URI:        http://www.c-point.co.th/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       CMatching
 * Domain Path:       /languages
 */

class CMatching {

	/**
	 * A reference to an instance of this class.
	 */
	private static $instance;

	/**
	 * The array of templates that this plugin tracks.
	 */
	protected $templates;

	/**
	 * Returns an instance of this class.
	 */
	public static function get_instance() {

		if ( null == self::$instance ) {
			self::$instance = new CMatching();
		}

		return self::$instance;

	}

	/**
	 * Initializes the plugin by setting filters and administration functions.
	 */
	private function __construct() {

		$this->templates = array();


		// Add a filter to the attributes metabox to inject template into the cache.
		if ( version_compare( floatval( get_bloginfo( 'version' ) ), '4.7', '<' ) ) {

			// 4.6 and older
			add_filter(
				'page_attributes_dropdown_pages_args',
				array( $this, 'register_project_templates' )
			);

		} else {

			// Add a filter to the wp 4.7 version attributes metabox
			add_filter(
				'theme_page_templates', array( $this, 'add_new_template' )
			);

		}

		// Add a filter to the save post to inject out template into the page cache
		add_filter('wp_insert_post_data',array( $this, 'register_project_templates' ));


		// Add a filter to the template include to determine if the page has our
		// template assigned and return it's path
		add_filter(	'template_include',array( $this, 'view_project_template'));


		// Add your templates to this array.
		$this->templates = array(
			'./templates/test.php' => 'Test',
		);

		add_action( 'init', array($this,'register_menus') );
	}
	public function register_menus() {

		$locations = array(
			'primary'  => __( 'Desktop Menu', 'cmatch' ),
		);
	
		register_nav_menus( $locations );
	}
	/**
	 * Adds our template to the page dropdown for v4.7+
	 *
	 */
	public function add_new_template( $posts_templates ) {
		$posts_templates = array_merge( $posts_templates, $this->templates );
		return $posts_templates;
	}

	/**
	 * Adds our template to the pages cache in order to trick WordPress
	 * into thinking the template file exists where it doens't really exist.
	 */
	public function register_project_templates( $atts ) {

		// Create the key used for the themes cache
		$cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

		// Retrieve the cache list.
		// If it doesn't exist, or it's empty prepare an array
		$templates = wp_get_theme()->get_page_templates();
		if ( empty( $templates ) ) {
			$templates = array();
		}

		// New cache, therefore remove the old one
		wp_cache_delete( $cache_key , 'themes');

		// Now add our template to the list of templates by merging our templates
		// with the existing templates array from the cache.
		$templates = array_merge( $templates, $this->templates );

		// Add the modified cache to allow WordPress to pick it up for listing
		// available templates
		wp_cache_add( $cache_key, $templates, 'themes', 1800 );

		return $atts;

	}

	/**
	 * Checks if the template is assigned to the page
	 */
	public function view_project_template( $template ) {
		// Return the search template if we're searching (instead of the template for the first result)
		if ( is_search() ) {
			return $template;
		}

		// Get global post
		global $post;

		// Return template if post is empty
		if ( ! $post ) {
			return $template;
		}

		// Return default template if we don't have a custom one defined
		if ( ! isset( $this->templates[get_post_meta(
			$post->ID, '_wp_page_template', true
		)] ) ) {
			return $template;
		}

		// Allows filtering of file path
		$filepath = apply_filters( 'page_templater_plugin_dir_path', plugin_dir_path( __FILE__ ) );

		$file =  $filepath . get_post_meta(
			$post->ID, '_wp_page_template', true
		);

		// Just to be safe, we check if the file exist first
		if ( file_exists( $file ) ) {
			return $file;
		} else {
			echo $file;
		}

		// Return template
		return $template;

	}

}
add_action( 'plugins_loaded', array( 'CMatching', 'get_instance' ) );



	/**
	 * create new user point table
	 */
	function NewTable(){
		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();
		$sql = array();
		
		$user_point = $wpdb->prefix . "user_point"; 
		if( $wpdb->get_var("show tables like '". $user_point . "'") !== $user_point ) {
			$sql[] = "CREATE TABLE $user_point (
				id mediumint(9) NOT NULL AUTO_INCREMENT,
				user_id int(7) NOT NULL,
				post_id int(7) NULL,
				user_point int(10	) NULL,
				wd_ref int(10	) NULL,
				time datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
				PRIMARY KEY  (id)
			) $charset_collate;";
		}
		
		$withdraw_req = $wpdb->prefix . "withdraw_req"; 
		if( $wpdb->get_var("show tables like '". $withdraw_req . "'") !== $withdraw_req ) {
			$sql[] = "CREATE TABLE $withdraw_req (
				id mediumint(9) NOT NULL AUTO_INCREMENT,
				user_id int(7) NOT NULL,
				status tinyint(2) NOT NULL,
				amount decimal(7,2) NOT NULL,
				type varchar(20) NULL,
				ref_id int(10) NULL,
				remark varchar(100) NULL,
				time datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
				PRIMARY KEY  (id)
			) $charset_collate;";
		}

		$chat_log = $wpdb->prefix . "chat_log"; 
		if( $wpdb->get_var("show tables like '". $chat_log . "'") !== $chat_log ) {
			$sql[] = "CREATE TABLE $chat_log (
				id mediumint(9) NOT NULL AUTO_INCREMENT,
				post_id int(7) NOT NULL,
				user_id int(7) NOT NULL,
				msg varchar(255) NULL,
				status tinyint(2) NOT NULL,
				time datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
				PRIMARY KEY  (id)
			) $charset_collate;";
		}

		$user_notification = $wpdb->prefix . "user_notification"; 
		if( $wpdb->get_var("show tables like '". $user_notification . "'") !== $user_notification ) {
			$sql[] = "CREATE TABLE $user_notification (
				id mediumint(9) NOT NULL AUTO_INCREMENT,
				user_id int(7) NOT NULL,
				l varchar(40) NOT NULL,
				msg varchar(255) NULL,
				status tinyint(2) NOT NULL,
				updated_at datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
				created_at datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
				PRIMARY KEY  (id)
			) $charset_collate;";
		}
		#Check to see if the table exists already, if not, then create it

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );

	}
